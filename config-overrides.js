/*
 * @Author: your name
 * @Date: 2021-04-02 15:32:40
 * @LastEditTime: 2021-09-03 11:21:36
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: /grain-and-oil-text-recognition/config-overrides.js
 */
const {
  override,
  fixBabelImports,
  addLessLoader,
  addWebpackAlias,
  overrideDevServer,
  addWebpackPlugin,
  addBabelPlugin,
  setWebpackPublicPath,
} = require('customize-cra');

const path = require('path');

const devServerConfig = () => config => {
  return {
    ...config,
    proxy: {
      // '/': {
      //   target: 'http://jslybms.ocr-cloud.hc.4paradigm.com/',
      //   changeOrigin: true,
      //   secure: false,
      // },
      '/scene-center': {
        target: 'http://10.104.1.142:8880/',
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          "^/scene-center": "/scene-center"
        }
      },
    },
  };
};
module.exports = {
  webpack: override(
    fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true,
    }),
    addWebpackAlias({
      '@': path.resolve(__dirname, 'src'),
    }),
    addBabelPlugin(['@babel/plugin-proposal-decorators', { legacy: true }])
    // setWebpackPublicPath('/oil-ticket')
  ),
  devServer: overrideDevServer(devServerConfig()),
};
