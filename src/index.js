/*
 * @Author: your name
 * @Date: 2021-02-24 12:08:48
 * @LastEditTime: 2021-03-16 14:36:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \oil-ticket\src\index.js
 */
import dva from 'dva';
import { createModel } from './model';
import createLoading from 'dva-loading';

// 1. Initialize
const app = dva({
  onError(ex) {
    // 会将 dva里的错误抛出
    console.log(ex);
  },
});

// 2. Plugins
app.use(createLoading());

// 3. Model
createModel(app);
// models.forEach(model => app.model(model));

// 4. Router
app.router(require('./App').default);

// 5. Start
app.start('#root');

// function openVConsole() {
//   const script = window.document.createElement('script');
//   script.src = 'http://wechatfe.github.io/vconsole/lib/vconsole.min.js?v=3.2.0';
//   document.body.append(script);
//   script.onload = () => {
//     window.VConsole && new window.VConsole();
//   };
// }

// openVConsole();
