import request, { uploadFile } from './request';

export function userRegister(data) {
  return request('/user/register', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

export function login(data) {
  return request('/scene-center/user/login', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 档案中心 tree
export function selectFolderInfo(data) {
  // /folder-info/getTreeFolderInfo
  return request('/folder-info/getTreeFolderInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 新增文件夹
export function addFolder(data) {
  return request('/folder-info/saveFolderInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 删除文件夹
export function delFolder(data) {
  return request('/folder-info/deleteFolderInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 查询数据管理列表  {"folderId":1}
export function selectDataManage(data) {
  return request('/data-manage/selectDataManage', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 票据类型
export function selectInvoiceType(data) {
  return request('/invoice-type/selectInvoiceType', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 添加识别的票据
export function saveInvoiceInfo(data) {
  return request('invoice-manage/saveInvoiceManage', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 删除票据
export function deleteTicket(data) {
  return request('/data-manage/deleteDataManage', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 上传
export function uploadTicket(data) {
  return uploadFile('/data-manage/uploadInvoice', data);
}

// 票据列表
export function getInvoiceList(data) {
  return request('/invoice-manage/selectInvoiceManage', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 查看票据详情
export function getInvoiceDetail(data) {
  return request('/invoice-manage/getInvoiceManage', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 部门树
export function getAllGroup(data) {
  return request('/group/getAllGroup', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 获取部门详情  1.{"id":"1"}
export function getGroupDetail(data) {
  return request('/group/getGroup', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 删除
export function deleteGroup(data) {
  return request('/group/deleteGroup', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 编辑
export function editGroup(data) {
  return request('/group/saveGroup', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// ---------用户管理----------
export function getUserList(data) {
  return request('/user/selectUser', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 禁用用户
export function userForbidden(data) {
  return request('/user/forbidden', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 请求参数{"username":"8889","password":"123","nickName":"好名字","groupId":"1","roleIds":[2]}
export function addUser(data) {
  return request('/user/register', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 编辑用户 请求参数{"id":"4","username":"8889","password":"123","nickName":"好名字","groupId":"1","roleIds":[2]}
export function editUser(data) {
  return request('/user/saveUser', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 删除用户
export function deleteUser(data) {
  return request('/user/deleteUser', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 重置密码 {"id":"1","newPassword":"123"}
export function resetPassword(data) {
  return request('/user/resetPassword', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 角色列表 请求参数{"groupId":"1"}
export function getRoleList(data) {
  return request('/role/selectRole', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 同意确认
export function agreeInvoiceManage(data) {
  return request('/invoice-manage/agreeInvoiceManage', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// -------data-manage-controller--------

// downloadFile
export function downloadFile(data) {
  return request(`/data-manage/down?${data}`);
}
