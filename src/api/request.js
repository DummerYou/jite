import fetch from 'dva/fetch';
import { notification } from 'antd';

function parseJSON(response) {
  return response.json();
}

const codeMessage = {
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

const errorHandler = response => {
  if (response && response.status) {
    if (response.status >= 200 && response.status < 300) {
      return response;
    }
    if ([403, 401].includes(response.status)) {
      window.location.hash = `#/login`;
    } else {
      const errorText = codeMessage[response.status] || response.statusText;
      const { status, url } = response;
      notification.error({
        message: `请求错误 ${status}: ${url}`,
        description: errorText,
      });
    }
  } else if (!response) {
    notification.error({
      description: '您的网络发生异常，无法连接服务器',
      message: '网络异常',
    });
  }

  return response;
};

let token = '';

export const updateToken = newToken => (token = newToken);

function request(url, options) {
  if (!token) {
    token = localStorage.getItem('TOKEN');
  }
  return fetch(url, {
    headers: {
      'content-type': 'application/json',
      Accept: 'application/json',
      token,
    },
    ...options,
  })
    .then(errorHandler)
    .then(parseJSON)
    .catch(err => ({ err }));
}

// 上传图片  multipart/form-data  application/x-www-form-urlencoded
export function uploadFile(url, options) {
  return fetch(url, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      token,
    },
    ...options,
  })
    .then(errorHandler)
    .then(parseJSON)
    .catch(err => ({ err }));
}
// 上传图片  multipart/form-data  application/x-www-form-urlencoded
export function downloadFile(url, options) {
  return fetch(url, {
    method: 'get',
    headers: {
      Accept: 'application/json',
      token,
    },
    responseType: 'blob',
    mode: 'cors',
    ...options,
  })
    .then(errorHandler)
    .then(parseJSON)
    .catch(err => ({ err }));
}

export function checkCode(res) {
  if (res.code === 401) {
    window.location.hash = `#/login`;
    return;
  }
  if (![0, 200,500].includes(res.code)) throw new Error(res.message);
}

export default request;
