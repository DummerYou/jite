import request, { uploadFile ,downloadFile} from './request';

// 查询场景列表
export function selectSceneInfo(data) {
  return request('/scene-center/scene/selectSceneInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 新增/修改场景（修改name、status值）
export function saveSceneInfo(data) {
  return request('/scene-center/scene/saveSceneInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 复制场景
export function copySceneInfo(data) {
  return request('/scene-center/scene/copySceneInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 查询模板列表
export function selectTemplateInfo(data) {
  return request('/scene-center/template/selectTemplateInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 新增/修改模板
export function saveTemplateInfo(data) {
  return request('/scene-center/template/saveTemplateInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 删除模板
export function deleteTemplateInfo(data) {
  return request('/scene-center/template/deleteTemplateInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// .删除场景
export function deleteSceneInfo(data) {
  return request('/scene-center/scene/deleteSceneInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 查看图片接口
export function fileImage(data) {
  return request('/scene-center/file/image?fileName='+data, {
    method: 'GET',
  });
}
// 保存/修改模板字段
export function saveBatchTemplateItem(data) {
  return request('/scene-center/templateItem/saveBatchTemplateItem', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 删除模板页
export function deleteTemplateItem(data) {
  return request('/scene-center/templateItem/deleteTemplateItem', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 删除模板字段
export function deleteTemplateItemField(data) {
  return request('/scene-center/templateItemField/deleteTemplateItemField', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 获取模板页+字段
export function selectTemplateItem(data) {
  return request('/scene-center/templateItem/selectTemplateItem', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 查询文件列表
export function selectFileInfo(data) {
  return request('/scene-center/fileInfo/selectFileInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 获取文件详情
export function getFileInfo(data) {
  return request('/scene-center/fileInfo/getFileInfo', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}

// 文件预览结果
export function previewFileField(data) {
  return request('/scene-center/fileInfo/previewFileField', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
// 识别图片对应文字
export function ocrText(data) {
  return request('/scene-center/ocr/ocrText', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}









export function uploadTicket(data) {
  return uploadFile('/scene-center/file/uploadFile', data);
}
export function downloadFile1(data) {
  return downloadFile(`/scene-center/fileInfo/downloadFile/${data.fileId}/0`);
}

export function downloadFile2(data) {
  return downloadFile(`/scene-center/fileInfo/downloadFile/${data.fileId}/1`);
}


// 文件处理
export function saveTask(data) {
  return uploadFile('/scene-center/partner/saveTask/'+data.taskId, data);
}

// 文件处理
export function getTask(data) {
  return request('/scene-center/partner/getTask/'+data.sceneId, {
    method: 'POST',
    // body: JSON.stringify(data),
  });
}