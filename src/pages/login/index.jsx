import React from 'react';
import { Divider, Form, Icon, Input, Button, message } from 'antd';
import { login } from '../../api/index';
import './index.css';
import { updateToken } from '../../api/request';

const FormItem = Form.Item;

function Login(props) {
  const onFinish = values => {
    login(values).then(res => {
      if (![0, 200].includes(res.code)) {
        message.error(res.message);
      } else {
        localStorage.setItem('TOKEN', res.data);
        document.cookie="token=" + res.data; 
        updateToken(res.data);
        props.history.push('/');
      }
    });
  };

  const onFinishFailed = errorInfo => {};
  return (
    <div className="container">
      <h1 className="titlename">金山粮油业务管理系统</h1>
      <Divider style={{ height: '2px' }} />
      <div className="content">
        <h2 className="login">账号密码登录</h2>
        <Form onFinish={onFinish} onFinishFailed={onFinishFailed}>
          账号
          <FormItem name="username" rules={[{ required: true, message: '请输入用户账号' }]}>
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="请输入用户账号"
            />
          </FormItem>
          密码
          <FormItem name="password" rules={[{ required: true, message: '请输入用户密码' }]}>
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="请输入用户密码"
            />
          </FormItem>
          <FormItem>
            <Button
              type="primary"
              htmlType="submit"
              style={{ width: '100%', marginLeft: 0, marginBottom: '50px' }}
            >
              登录
            </Button>
          </FormItem>
        </Form>
      </div>
      <span className="bottom-text">Copyright  2021  上海市金山区粮油购销有限公司</span>
    </div>
  );
}

export default Login;
