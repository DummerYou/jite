import React, { PureComponent } from 'react';
import {
  Pagination,
  Row,
  Col,
  Button,
  Input,
  Select,
  message,
  Icon,
  Upload,
  Space,
  Menu,
  Drawer,
  Dropdown,
  Spin,
  Modal,
  Form,
  Radio,
  ConfigProvider,
} from 'antd';
import { connect } from 'dva';
import ImageViewCanvas from "../ImageView/ImageViewCanvas"
import { ref } from 'react.eval';
import { PlusOutlined, EllipsisOutlined, UploadOutlined ,ExclamationCircleOutlined,
  InboxOutlined,} from '@ant-design/icons';
import './index.scss';
import { saveTask } from '../../../../api/scene';
const { Option } = Select;
const { Search } = Input;
const { TextArea } = Input;
const { Dragger } = Upload;
const { confirm } = Modal;
@connect(({ sceneModel, loading }) => ({ ...sceneModel, loading }))
class Preview extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      isModalVisible1: false,
      fileList: [],
      selectedRowKeys: [],
      datas:[],
      taskId:"",
      imgUrl: '/scene-center/file/image?fileName=',
    };
    this.addForm = React.createRef();
    ref(this);
  }

  async componentDidMount() {
    try {
    } catch (error) {
      error.message && message.error(error.message);
    }
  }
  showModal1 = () =>{
    const { dispatch ,dataList,imgDatas } = this.props;
    if(dataList && dataList[0]){
      console.log(dataList[0].taskId)
      this.setState({
        isModalVisible1:true,
        taskId:dataList[0].taskId,
      },()=>{
        
      })
    }
  }
  showModal = async () =>{
    const {imgUrl} = this.state
    const { dispatch ,dataList,imgDatas } = this.props;
    console.log(dataList)
    try {
      let arrList = []
      for (let i = 0; i < dataList.length; i++) {
        let res = await dispatch({
          type: 'sceneModel/previewFileField',
          payload: {
            id: dataList[i].taskId,
          },
        });
        if(res.data && res.data[0]){
          let list = [];
          for (const key in res.data) {
            list.push(res.data[key]) 
          }
          arrList = arrList.concat(list)
        }
      }
      console.log(arrList)
      console.log(imgDatas)
      let num = 10;
      arrList = JSON.parse(JSON.stringify(arrList)).map((vv,i) => {
        console.log(vv)
        let arrListss = vv.map(v=>{
          v.tagId = v.tag+""
          v.tagType = v.type+""
          try {
            let position = JSON.parse(v.position)
            let imgGets = new ImageViewCanvas();
            return imgGets.getStructure(position[0], position[1], position[2], position[3], v)
          } catch (error) { 
            console.log(error)
            return v
          }
        })
        return {
          url:imgDatas[i],
          data:{itemFieldList:arrListss}
        }

          
      })
      console.log(arrList)
      
      this.setState({
        isModalVisible:true,
        datas:arrList,
      },()=>{
        arrList.forEach((v,i)=>{
          let layers = []
          if(v.data){
            layers = v.data.itemFieldList
          }
          let canvas = new ImageViewCanvas({imgUrl:'http://10.104.1.142:8880' +  imgUrl + v.url,layers:layers},"previews"+i)
          
        })
      })
    } catch (e) { 
      console.log(e)
    }
  }
  saveTaskClick = async () =>{
    const {taskId,fileList,selectedRowKeys} = this.state
    const { dispatch  } = this.props;
    console.log(taskId)
    console.log(fileList)
    console.log(fileList[0])
    console.log(fileList[0].raw)
    console.log(selectedRowKeys)
    try {
      const formData = new FormData();
      if (!fileList.length) {
        message.error('请选择文件')
        return;
      }
      formData.append('file', fileList[0]);
      const res = await saveTask({ body: formData,taskId });
      if(res.code == 200){
        res.message ? message.success(res.message) : message.success('上传成功');
      }else{
        res.message && message.error(res.message);
      }
    } catch (error) {
      console.log(error)
    }
  }
  onSearch = async (e) =>{
    const { dispatch  } = this.props;
    try {
      if (!e) {
        message.error('请输入任务ID')
        return;
      }
      console.log(e)
      let res = await dispatch({
        type: 'sceneModel/getTask',
        payload: {
          sceneId: e,
        },
      });
      if(res.code == 200){
        this.setState({
          getTaskVal:JSON.stringify(res.data)
        },()=>{
        })
      }else{
        res.message && message.error(res.message);
      }
    } catch (error) {
      console.log(error)
    }
    
    
  }
  showModal2 = () => {
    console.log("object")
    const {imgUrl} = this.state
    const { imgList ,saveTemplate,allList,getdataNumIndex,imgDatas} = this.props;
    let list = JSON.parse(JSON.stringify(imgDatas)).map(i => {
      return {url:i}
    });

    saveTemplate(999,(v)=>{
      console.log(v)
      console.log(allList)
      v.forEach(vv=>{
        console.log(getdataNumIndex(vv.fileId, vv.imgIndex))
        let num = getdataNumIndex(vv.fileId, vv.imgIndex)
        list[num].data = vv;
      })
      console.log(list)
      
    })
    console.log(imgDatas)

    this.setState({
      isModalVisible:true,
      datas:list,
    },()=>{
      list.forEach((v,i)=>{
        let layers = []
        if(v.data){
          layers = v.data.itemFieldList
        }
        let canvas = new ImageViewCanvas({imgUrl:'http://10.104.1.142:8880' +  imgUrl + v.url,layers:layers},"previews"+i)
        
      })
    })
  }
  

  render() {
    const {
      isModalVisible,
      isModalVisible1,
      datas,
      fileList,
      taskId,
      getTaskVal,
    } = this.state;
    const _this = this;
    const {  } = this.props;

    const handleCancel = () => {
      console.log("object")
      this.setState({
        isModalVisible:false,
        isModalVisible1:false,
        datas:[],
      })
    };
    
    const props = {
      
      onRemove: file => {
        this.setState(state => {
          const index = state.fileList.indexOf(file);
          const newFileList = state.fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: file => {
        this.setState(state => ({
          fileList: [ file],
          selectedRowKeys: [ file.uid],
        }));
        console.log(file);

        // this.setState({ selectedRowKeys });
        return false;
      },
      onChange: info => {
        console.log(info);
        const event = info.event;
        console.log(event);
      },
      fileList,
    };
    

    return (
      <>
      <Modal title="预览" width="80vw" visible={isModalVisible} footer={null} onCancel={handleCancel}>
        {datas.map((v,i)=>{
          return (<div className='previews'>
            <div className='previewsL' id={'previews'+i}></div>
            <div className='previewsR'>
              <p>{i+1}</p>
              {
                v.data?((v.data.itemFieldList && v.data.itemFieldList.length)?v.data.itemFieldList.map(v=>(
                  //       "name":v.name,
                  // "tagId": v.tagId,	//0.表头 1.表体 2.表尾
                  // "position": `[${v.x1},${v.y1},${v.x2},${v.y2}]`,		//标记字段坐标
                  // "type": v.tagType,	//类型，标记，需要imgPageNum和position参数
                        <div className='previewsRs'>
                          <div>字段名：{v.name}</div>
                          <div>类型：{v.tagType=='0'?'标记':v.tagType=='1'?'全文匹配':''}</div>
                          {v.tagType=='0'?<div>字段值：{v.value}</div>:null}
                          <div>字段类型：{v.tagId=='0'?'表头':v.tagId=='1'?'表体':v.tagId=='2'?'表尾':''}</div>
                        </div>
                      )):(<p>暂无标记<br />请添加标记</p>)):(<p>暂无可预览标记,<br />请点击当前页后查看</p>)
              }
            </div>
          </div>)
        })}
      </Modal>
      <Modal title="文件处理" width="600px" visible={isModalVisible1} footer={null} onCancel={handleCancel} wrapClassName="Modalccc">
        <Row style={{ marginBottom: '10px' }}>
            <Col span={4}>场景ID:</Col>
            <Col span={20}><Input value={taskId} onChange={e => this.setState({ taskId: e.target.value })}/></Col>
          </Row>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={4}>文件上传:</Col>
            <Col span={20}>
              <Dragger {...props} showUploadList={true} maxCount={1}>
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">请将图像压缩包文件拖拽至此上传或点击上传</p>
                  <p className="ant-upload-hint" style={{ margin: '0 54px' }}>
                    支持的数据类型：pdf，图片格式（.jpg .jpeg .png
                    .bmp）,支持以.zip格式上传数据，且文件大小不超过2G
                  </p>
                </Dragger>
            </Col>
          </Row>
        <Row style={{ marginBottom: '10px' }}>
            <Col span={4}></Col>
            <Col span={20}><Button type="primary"  onClick={this.saveTaskClick}>提交</Button></Col>
          </Row>
        <Row style={{ marginBottom: '10px' }}>
            <Col span={4}>任务ID:</Col>
            <Col span={20}>
              <Search
                allowClear
                enterButton="查询"
                onSearch={this.onSearch}
              />
              </Col>
          </Row>
        <Row style={{ marginBottom: '10px' }}>
            <Col span={4}></Col>
            <Col span={20}>
              <TextArea
                value={getTaskVal}
                placeholder=""
                autoSize={{ minRows: 3, maxRows: 5 }}
              />
              </Col>
          </Row>
          
      </Modal>
    </>
    );
  }
}

export default Preview;
