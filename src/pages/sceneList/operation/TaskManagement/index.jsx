import React, { useRef } from 'react';
// import { observer } from 'mobx-react-lite';
import { Input, message, Modal,Button,Select  } from 'antd';
import { LeftOutlined ,RightOutlined} from '@ant-design/icons';
import { ref } from 'react.eval';
// import { addWorkSpaceIdToUrlV2 } from 'src/common/util';
// import leftIcon from 'src/common/resource/image/left.svg';
// import rightIcon from 'src/common/resource/image/right.svg';
// import leftDisabledIcon from 'src/common/resource/image/left_disabled.svg';
// import rightDisabledIcon from 'src/common/resource/image/right_disabled.svg';
// import { useRouteParams } from 'src/react/components/labelstudio/common/hooks';
import './index.scss';
import { downloadFile1,downloadFile2 } from '@/api/scene';


const { Option } = Select;

export default function TaskManagement(props) {
    const { afterSave, loading, currentTask,dataNum, total, jumpTask, isChange,saveTemplate, ids,allList,saveLabels, clearStates,isFirstPage,isLastPage,recordsList,recordsKey,getRecordsItem} = props;
    // const jumpTaskInput = useRef(null);
    // const isFirstPage = props.isFirstPage();
    // const isLastPage = props.isLastPage();
    // const history = useHistory();
    // useCustomHook
    // const routeParams = useRouteParams();
    // const { cycleId } = routeParams;

    function jumpCycleSetting() {
        // 取消操作
        window.location.href = window.location.href.split('/operation')[0];
    }
    // 返回
    function toSceneManagement() {
        jumpOtherTask(() => {
            // 保存操作
            // saveLabels();
            jumpCycleSetting();
        }, jumpCycleSetting);
    }
    function jumpOtherTask(saveCb, cb) {
        console.log(isChange);
        if (isChange) {
            Modal.confirm({
                title: '请确认是否保存当前操作？',
                onOk: () => {
                    saveCb();
                },
                onCancel: () => {
                    cb();
                },
            });
        } else {
            cb();
        }
    }
    function download(file, name) {
        let url = window.URL.createObjectURL(file); // 3.创建一个临时的url指向blob对象
        let a = document.createElement("a");
        a.href = url;
        a.download = name;
        a.click();
        window.URL.revokeObjectURL(url);
    }

    function switchTask(e) {
        const index = e.target.value;
        const pattern = /^([1-9][0-9]*)$/;
        if (index < 0 || index > total || !pattern.test(index)) {
            message.error('请输入正确的任务');
        } else if (index - 1 !== currentTask) {
            const cb = async () => {
                // afterSave();
                jumpTask(index - 1);
              
            };
            jumpOtherTask(() => save(cb), cb);
        }
    }
    async function save(cb) {
        const res = {
            status:"0"
        };
        // FIXED ISSUE: ensure image model cleared before callback function
        if (res && res.status === '0') {
            cb();
        }
    }
    

    function toPrevious(){
        console.log(isFirstPage() , loading);
        if (isFirstPage() || loading) {
            return false;
        }
        console.log("object");
        const cb = async () => {
            // afterSave();
            const nextIndex = currentTask - 1;
            jumpTask(nextIndex);
        };
        jumpOtherTask(() => save(cb), cb);
    }

    function toNext() {
        console.log(loading);
        if (loading) {
            return false;
        }
        const cb = async () => {
            if (!isLastPage()) {
                // afterSave();
                const nextIndex = currentTask + 1;
                jumpTask(nextIndex);
            } else {
                // setLastTaskModalInfoVisible(true);
            }
        };
        jumpOtherTask(() => save(cb), cb);
    }
    function handleChange(value) {
        console.log(`selected ${value}`);
        getRecordsItem(value)
    }
    function openUrl() {
        console.log(allList)
        console.log(allList.sceneId,allList.templateId)
        console.log(ids,dataNum[currentTask])
        window.location.href = window.location.href.split('/operation')[0] + `/template?id=${ids}&fileId=${dataNum[currentTask]}`;
    }
    return (
        
           
        <div className='label-tool-operations'>
            <div className='to-scene'>
                <span className='hypc-link-span' onClick={toSceneManagement}>
                    <LeftOutlined style={{ marginRight: 4 }} />
                    返回场景中心
                </span>
            </div>
            <div className='gotorecordsList'>
            
            <Button type="primary" onClick={openUrl}>查看模板</Button>
            <Button type="primary" onClick={()=>ref('labelImage.showModal4')}>添加模板</Button>
            <Button type="primary" onClick={()=>{saveTemplate(true)}}>保存</Button>
            <Button type="primary" onClick={()=>ref('preview.showModal')}>预览</Button>
            <Button onClick={()=>{window.open(`/scene-center/fileInfo/downloadFile/${dataNum[currentTask]}/0`)}}>文件内容下载</Button>
            <Button onClick={()=>{window.open(`/scene-center/fileInfo/downloadFile/${dataNum[currentTask]}/1`)}}>文件明细下载</Button>
            <Button type="primary" onClick={()=>ref('preview.showModal1')}>文件处理</Button>

            
            </div>
            <div className='task-operation'>
               
                <Button icon={<LeftOutlined />} onClick={toPrevious} disabled={isFirstPage()}/>
            
                <span className='task-operation-item current-task'>
                    当前任务
                    <span className='task-page' style={{ marginLeft: 4 }}>
                        {currentTask + 1 }
                    </span>
                    <span className='task-page' style={{ margin: '0 4px' }}>
                        /
                    </span>
                    <span className='task-page'>{total}</span>
                </span>
              
                <Button icon={<RightOutlined />} onClick={toNext}  disabled={isLastPage()}/>
              
                <span className='task-operation-item jump-task-span'>
                    快速跳转
                    <Input
                        style={{ width: 100 }}
                        // ref={jumpTaskInput}
                        onPressEnter={switchTask}
                        placeholder='enter键跳转'
                    />
                    任务
                </span>
            </div>
        </div>
    );
};

