import React, { PureComponent } from 'react';
import {
  Pagination,
  Row,
  Col,
  Button,
  Input,
  Select,
  message,
  Icon,
  Upload,
  Space,
  Menu,
  Drawer,
  Dropdown,
  Spin,
  Modal,
  Form,
  Radio,
  ConfigProvider,
} from 'antd';
import { connect } from 'dva';
import { ref } from 'react.eval';
import { PlusOutlined, EllipsisOutlined, UploadOutlined ,ExclamationCircleOutlined} from '@ant-design/icons';
import './index.scss';
const { Option } = Select;
const { Search } = Input;
const { TextArea } = Input;
const { confirm } = Modal;
@connect(({ sceneModel, loading }) => ({ ...sceneModel, loading }))
class LabelImage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible1: false,
      current: {},
      form1: {},
      openItem: {},
      isModalVisible2: false,
      isModalVisible3: false,
      isModalVisible4: false,
      handlerScript: '',
      wastePageHandlerScript: '',
      backHandlerScript: '',
      thisId:null,
      addModal:false,
      templateName:"",
      templateName2:"",
      matchingHandlerScript:'',
      matchingHandlerScript3:"",
      imgUrl: '/scene-center/file/image?fileName=',
    };
    this.addForm = React.createRef();
    ref(this);
  }

  async componentDidMount() {
    console.log('object------------++++++++++++++++++++');
    console.log(this.props);
    const { setImgList, imgList, dispatch,recordsItem } = this.props;

    try {
      
      // const res = await dispatch({
      //   type: 'sceneModel/selectTemplateInfo',
      //   payload: {
      //     "sceneId": "1",//页码图路径
      //   },
      // });
      // console.log(res);
    } catch (error) {
      error.message && message.error(error.message);
    }
  }
  setThisId=(num)=>{
    this.setState({
      thisId:num
    })
  }
  setImgDetail=(id)=>{
    const {setImgList, imgList, } = this.props;
    let list = imgList;
    let index = null;
    imgList.forEach((v,i)=>{
      if(v.id == id){
        index = i
      }
    })
    if(index !== null){
      list.splice(index,1)
      this.setState({
        thisId:null,
      })
    }
    setImgList(list)
  }
  deleteData = async() =>{
    const {thisId} = this.state
    const {dispatch, setImgList, imgList, } = this.props;
    try {
        
      // if(thisId<100000){
      //   const res = await dispatch({
      //     type: 'sceneModel/deleteTemplateItemField',
      //     payload:{
      //       id:thisId,
      //     },
      //   });
      //   console.log(res);
      //   if(res.code == 200){
      //     message.success('删除成功');
      //     this.setImgDetail(thisId)
      //   }
      // }else{
      //   this.setImgDetail(thisId)
      //   message.success('删除成功');
      // }
      this.setImgDetail(thisId)
      message.success('删除成功');
      
    } catch (error) {
      if(thisId<100000){
        error.message && message.error(error.message);
      }
    }
  }
  
  getdataNumIndex(v){
    const {dataNum} = this.props
    var bs=dataNum.map((item,index)=>{
        return item={
        x:index,
        y:item
    }
    })
    var k=bs[v]
    var m=bs.filter(item=>item.y==k.y)
    if(m.findIndex(item=>item.x==k.x) != -1){
      return m.findIndex(item=>item.x==k.x)
    }else{
      return false
    }
  }
  saveTemplate = async() =>{
    const {dispatch,  imgList, imgDatas, currentTask, dataNum ,recordsItem,jumpTask ,total } = this.props;
    console.log("保存模板");
    console.log(imgList);
     
    try {
      let imgIndex = this.getdataNumIndex(currentTask)
      let payload = [{
        "imgIndex": imgIndex,//页码索引
        "itemFieldList": imgList.map((v)=>{
          return {
            "name":v.name,
            "tagId": v.tagId,	//0.表头 1.表体 2.表尾
            "position": `[${v.x1},${v.y1},${v.x2},${v.y2}]`,		//标记字段坐标
            "type": v.tagType,	//类型，标记，需要imgPageNum和position参数
            "handlerScript": v.handlerScript	//处理脚本
          }
        }),
        "templateId": recordsItem.id,	//模板id
        "fileId": dataNum[currentTask],	//文件id
        "name": recordsItem.name,	//名称
        "matchingHandlerScript": recordsItem.matchingHandlerScript	//模板匹配逻辑脚本
      }]
      const res = await dispatch({
        type: 'sceneModel/saveBatchTemplateItem',
        payload,
      });
      console.log(res);
      if(res.code == 200){
        message.success('保存成功');
        if(currentTask<total-1){
          jumpTask(currentTask+1)
        }
      }
    } catch (error) {
      error.message && message.error(error.message);
    }
  }

  pubTemplate = async () =>{
    const { dispatch,ids} = this.props;
    const _this = this;
    confirm({
      title: '应用是否上线',
      icon: <ExclamationCircleOutlined />,
      okText: '确定',
      cancelText: '取消',
      onOk() {
        _this.shangClick();
      },
      onCancel() {
        console.log('Cancel');
      },
    });
    
  }
  shangClick = async () =>{
    const { dispatch,ids} = this.props;
    try {
      const res = await dispatch({
        type: 'sceneModel/saveSceneInfo',
        payload:{
          id: ids,
          status: 1,
        },
      });
      if (res.code == 200) {
        message.success('发布成功');
        window.location.href = window.location.href.split('/operation')[0];
      }
    } catch (error) {
      error.message && message.error(error.message);
    }
  }
  getImgBase =  (url,height,width,x,y) => {
    const { imgUrl } = this.state;
    return new Promise(resolve => {
      var canvas = document.createElement('canvas');
      var context = canvas.getContext('2d'); 
      //返回一个用于在画布上绘图的环境，当前唯一的合法值是 "2d"，它指定了二维绘图，返回一个 CanvasRenderingContext2D 对象，使用它可以绘制到 Canvas 元素中


      var img = new Image();
      img.crossOrigin = 'Anonymous'; //解决Canvas.toDataURL 图片跨域问题
      img.src ='http://10.104.1.142:8880' +  imgUrl + url;
      console.log(imgUrl + url)
      console.log("object")
      //渲染方法
      img.onload = function () {
          console.log("object2")
          canvas.width = width;
          canvas.height = height;
          context.drawImage(img, x, y, width, height, 0, 0, width, height);
          var base64Img = canvas.toDataURL('image/jpg');
          console.log(base64Img);
          resolve(base64Img)
      }
  });
        
  }
  showModal = async currentOld => {
    console.log(currentOld);
    let current = JSON.parse(JSON.stringify(currentOld))
    const { dispatch, imgDatas, currentTask, dataNum ,imgList,setImgList } = this.props;
    try {
      console.log(this.getImgBase)
      const fileBase = await this.getImgBase(imgDatas[currentTask],current.height,current.width,current.x1,current.y1)
      console.log(fileBase)
      const res = await dispatch({
        type: 'sceneModel/ocrText',
        payload: {
          base64Encoder: fileBase,
        },
      });
      console.log(res);
      if(res.code == 200){
        current.value = res.data
      }
      console.log(current);
    } catch (e) { 
      console.log(e)
    }
    
    // let values = 
    // // asdasssdsa
    // console.log(values);
    // let newCurrent = Object.assign(current, values);
    // console.log(newCurrent);
    // if(!newCurrent.handlerScript){
    //   newCurrent.handlerScript = "return text.substring(0,1)"
    // }
    console.log("object99999999999999999999999999999999999")
    console.log(imgList)
    console.log(current)
    let newImgList = [];
    let imgListEachState = true;
    imgList.forEach(v => {
      if (v.id == current.id) {
        current.name = v.name;
        current.handlerScript = v.handlerScript;
        current.tagId = v.tagId;
        current.tagType = v.tagType;
        
        newImgList.push(current);
        imgListEachState = false;
      } else {
        newImgList.push(v);
      }
    });
    // if (imgListEachState) {
    //   newImgList.push(current);
    // }

    console.log(newImgList);
    setImgList(newImgList);



    // this.setState({ isModalVisible1: true, current: JSON.parse(JSON.stringify(current)) }, () => {
    //   setTimeout(() => {
    //     console.log(this.addForm);
    //     this.addForm.current.setFieldsValue(JSON.parse(JSON.stringify(current)));
    //   }, 500);
    // });
  };
  addTextList = () =>{
    const { dispatch, imgDatas, currentTask, dataNum ,imgList,setImgList } = this.props;
    let newImgList = imgList?JSON.parse(JSON.stringify(imgList)):[];
    let noState = imgList.some(v => {
      if(!v.name){
        return true
      }
    });
    console.log(noState)
    if(noState){
      message.warning('请设置上一个添加的字段名');
      return
    }
    let newImg = {
      "tagType": "0",
      id:Math.floor(Math.random()*(1000000-100000+1)+100000),
      "handlerScript": "",
      "tagId": "0"
  }
    newImgList.push(newImg)
    this.setThisIdList(newImg)
    setImgList(newImgList);
  }
  setThisIdList = (v) =>{
    this.setState({thisId:v.id})
    ref('imageView.setCurrent', v);
  }
  showModal2 = current => {
    this.setState({ isModalVisible2: true, handlerScript: current.handlerScript || '' }, () => {
      this.setopenItem(JSON.parse(JSON.stringify(current)));
    });
  };
  setopenItem(item) {
    this.setState({
      openItem: item,
    });
  }
  showModal3 = () => {
    const { recordsItem } = this.props;
    this.setState({
      addModal:false,
      isModalVisible3: true,
      wastePageHandlerScript: recordsItem.wastePageHandlerScript,
      backHandlerScript: recordsItem.backHandlerScript,
      matchingHandlerScript:recordsItem.matchingHandlerScript || "",
      templateName:recordsItem.name,
    });
  };
  showModal4 = (recordsItem={}) => {
    this.setState({
      addModal:true,
      isModalVisible3: true,
      wastePageHandlerScript: recordsItem.wastePageHandlerScript || "",
      backHandlerScript: recordsItem.backHandlerScript || "",
      matchingHandlerScript:recordsItem.matchingHandlerScript || "",
      templateName2:recordsItem.name || "",
    });
  };
  showModal44 = () => {
    const { recordsItem,matchingHandlerScript2 } = this.props;
    console.log(matchingHandlerScript2)
    const reg = /\"(.*?)\"/;
    let matchingHandlerScript3 = "";
    if(reg.exec(matchingHandlerScript2) && reg.exec(matchingHandlerScript2)[1]){
      matchingHandlerScript3 = reg.exec(matchingHandlerScript2)[1]
    }
    this.setState({
      isModalVisible4: true,
      matchingHandlerScript3,
    });
  };
  getMatching = () => {
    const { openItem,handlerScript } = this.state;

    // this.setState({
    //   handlerScript: "暂无接口",
    // });
  };

  render() {
    const {
      isModalVisible1,
      current,
      isModalVisible2,
      isModalVisible4,
      openItem,
      handlerScript,
      wastePageHandlerScript,
      matchingHandlerScript,
      matchingHandlerScript3,
      backHandlerScript,
      isModalVisible3,
      thisId,
      addModal,
      templateName,
      templateName2,
    } = this.state;
    const _this = this;
    const { setImgList, imgList,recordsItem,recordsList,recordsKey,getRecordsItem } = this.props;
    const handleOk = () => {
      console.log(imgList);
      console.log(this.addForm);
      // this.setState({ isModalVisible1: false });
      this.addForm.current
        .validateFields()
        .then(values => {
          console.log(current);
          let newCurrent = Object.assign(current, values);
          console.log(newCurrent);
          if(!newCurrent.handlerScript){
            newCurrent.handlerScript = ""
          }

          let newImgList = [];
          let imgListEachState = true;
          imgList.forEach(v => {
            if (v.id == newCurrent.id) {
              newImgList.push(newCurrent);
              imgListEachState = false;
            } else {
              newImgList.push(v);
            }
          });
          if (imgListEachState) {
            newImgList.push(newCurrent);
          }

          console.log(newImgList);
          setImgList(newImgList);

          this.addForm.current.setFieldsValue({});
          this.addForm.current.resetFields();
          handleCancel(values);
        })
        .catch(info => {
          console.log('Validate Failed:', info);
        });
    };
    const setImgListTextVal = (e, i, name) => {
      console.log(e);
      let newimgList = imgList;
      newimgList[i][name] = e;
      setImgList(newimgList, true);
    };

    const handleCancel = () => {
      console.log(imgList);
      setImgList();
      this.addForm.current.resetFields();
      this.setState({ isModalVisible1: false });
    };
    const handleOk2 = () => {
      let index = '';
      imgList.forEach((v, i) => {
        if (v.id == openItem.id) {
          index = i;
        }
      });
      setImgListTextVal(handlerScript, index, 'handlerScript');
      this.setState({ isModalVisible2: false });
    };
    const handleCancel2 = () => {
      this.setState({ isModalVisible2: false });
    };
    const handleOk3 = async() => {
      const { recordsItem,setrecordsItem,dispatch,ids,dataNum,currentTask  } = this.props;
      console.log(ids,dataNum[currentTask])
      try {
        let payloda = {
          id:recordsItem.id,
          fileId: dataNum[currentTask],
          sceneId: ids, //场景id
          wastePageHandlerScript,
          matchingHandlerScript,
          backHandlerScript,
          name:templateName,
        }
        if(addModal){
          payloda = {
            fileId: dataNum[currentTask],
            sceneId: ids, //场景id
            wastePageHandlerScript,
            matchingHandlerScript,
            backHandlerScript,
            name:templateName2,
          }
        }
        console.log(payloda)
        if(!payloda.name){
          message.error('请输入模板名称');
          return 
        }
        const res2 = await dispatch({
          type: 'sceneModel/saveTemplateInfo',
          payload: payloda,
        });
        if (res2 && res2.code == 200) {
          this.setState({
            isModalVisible3: false,
          });
          if(addModal){
            setrecordsItem(payloda)
          }else{
            setrecordsItem(Object.assign(recordsItem,payloda))
          }
        }
        
      } catch (e) {
        
      }
    };
    const handleCancel3 = () => {
      
      this.setState({ isModalVisible3: false });
    };
    const handleChange = (value) => {
        console.log(`selected ${value}`);
        getRecordsItem(value)
    }
    const handleCancel4 = () => {
      
      this.setState({ isModalVisible4: false ,matchingHandlerScript3:""});
    };
    
    const handleOk4 = async() => {
      const { recordsItem,setrecordsItem,dispatch } = this.props;
      try {
        let payloda = {
          id:recordsItem.id,
          matchingHandlerScript3,
        }
        setrecordsItem(Object.assign(recordsItem,payloda),true)
        handleCancel4()
        // const res2 = await dispatch({
        //   type: 'sceneModel/saveTemplateInfo',
        //   payload: payloda,
        // });
        // if (res2 && res2.code == 200) {
        //   this.setState({
        //     isModalVisible3: false,
        //   });
        //   setrecordsItem(Object.assign(recordsItem,payloda))
        //   handleCancel4()
        // }
        
      } catch (e) {
        
      }
    };

    return (
      <div className="labelImage">
        <div className="labelImageTitle">
          {<Select placeholder="请选择" defaultValue={recordsKey || null} style={{ width: 120 }} onChange={handleChange} key={recordsKey}>
                {recordsList.length?recordsList.map(vv=>{return <Option value={vv.id} key={vv.id}>{vv.name}</Option>}):null}
            </Select>}
          <Button type="link" onClick={this.addTextList}>
            添加字段
          </Button>
        </div> 
        <div className="lableImageList">
          {imgList.length
            ? imgList.map((v, i) => {
                if(v){
                  return (
                    <div key={v.id} className={v.id == thisId?'lableImageLists this':'lableImageLists'} onClick={()=>{this.setThisIdList(v)}}>
                      <ConfigProvider componentSize="small">
                        <Row>
                          <Col span={8}>字段名:</Col>
                          <Col span={16}>
                            <Input
                              value={v.name}
                              onChange={e => setImgListTextVal(e.target.value, i, 'name')}
                            />
                          </Col>
                        </Row>
                        <Row>
                          <Col span={8}>类型:</Col>
                          <Col span={16}>
                            <Select value={v.tagType} style={{ width: '100%' }}
                              onChange={e => setImgListTextVal(e, i, 'tagType')}>
                              <Option value="0">标记</Option>
                              <Option value="1">全文匹配</Option>
                            </Select>
                          </Col>
                        </Row>
                        {
                          v.tagType == 0?
                          <Row>
                            <Col span={8}>字段值:</Col>
                            <Col span={16}>
                              <Input
                                type="textarea"
                                value={v.value}
                                onChange={e => setImgListTextVal(e.target.value, i, 'value')}
                              />
                            </Col>
                          </Row>
                          :null
                        }
                        <Row>
                          <Col span={8}>坐标:</Col>
                          <Col span={16}>
                            <Input
                              type="textarea" disabled
                              value={v.x1?`${v.x1},${v.y1},${v.x2},${v.y2}`:""}
                            />
                          </Col>
                        </Row>
                        <Row>
                          <Col span={8}>字段类型:</Col>
                          <Col span={16}>
                            <Radio.Group
                              value={v.tagId}
                              onChange={e => setImgListTextVal(e.target.value, i, 'tagId')}
                            >
                              <Radio value="0">表头</Radio>
                              <Radio value="1">表体</Radio>
                              <Radio value="2">表尾</Radio>
                            </Radio.Group>
                          </Col>
                        </Row>
                        <Row>
                          <Col span={8}>脚本函数:</Col>
                          <Col span={8}>
                            <Button type="link" onClick={() => this.showModal2(v)}>
                              编辑
                            </Button>
                          </Col>
                          <Col span={8} style={{textAlign: "right",paddingRight:"15px"}}>
                            <Button type="link" onClick={() => {
                              confirm({
                                title: '是否删除：' + (v.name || ""),
                                icon: <ExclamationCircleOutlined />,
                                okText: '确定',
                                cancelText: '取消',
                                onOk() {
                                  _this.setImgDetail(v.id);
                                },
                                onCancel() {
                                  console.log('Cancel');
                                },
                              });
                              }} danger>
                              删除
                            </Button>
                          </Col>
                        </Row>
                      </ConfigProvider>
                    </div>
                  );
                }
              })
            : null}
        </div>
        
        <div className="labelImageBottom">
          <Button type="link" onClick={this.showModal44}>当前页匹配脚本函数</Button>
          {/* <Button disabled={!thisId} onClick={this.deleteData}>删除该份数据</Button> */}
          {/* <Button type="primary" onClick={this.saveTemplate}>保存模板</Button>
          <Button type="primary" onClick={this.pubTemplate}>发布模板</Button> */}
        </div>
        <Modal
          visible={isModalVisible1}
          title="添加字段标注"
          onCancel={handleCancel}
          onOk={handleOk}
        >
          <Form
            ref={this.addForm}
            layout="Horizontal"
            name="form_in_modal"
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 20 }}
            initialValues={{}}
          >
            <Form.Item
              name="name"
              label="字段名"
              rules={[{ required: true, message: '请输入字段名!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item name="value" label="字段值">
              <Input type="textarea" />
            </Form.Item>
            <Form.Item name="tagId" label="字段类型">
              <Radio.Group>
                <Radio value="0">表头</Radio>
                <Radio value="1">表体</Radio>
                <Radio value="2">表尾</Radio>
              </Radio.Group>
            </Form.Item>
          </Form>
        </Modal>

        <Modal visible={isModalVisible2} title={"字段名：" + (openItem.name || "")} onCancel={handleCancel2} className={'handlerScriptModal'} onOk={handleOk2}>
          <span style={{lineHeight: "32px"}}>function(data,text)</span>
          <TextArea
            rows={4}
            value={handlerScript}
            placeholder="编辑脚本函数"
            onChange={e => this.setState({ handlerScript: e.target.value })}
          />
        </Modal>

        
        <Modal visible={isModalVisible4} title="当前脚本编辑：" className={'handlerScriptModal'} onCancel={handleCancel4} onOk={handleOk4}>
          <span style={{lineHeight: "32px"}}>文本中包含以下字段则进行字段匹配</span>
          <TextArea
            rows={4}
            placeholder="编辑脚本函数"
            value={matchingHandlerScript3}
            onChange={e => this.setState({ matchingHandlerScript3: e.target.value })}
          />
        </Modal>


        <Modal visible={isModalVisible3} title="添加模板" onCancel={handleCancel3} onOk={handleOk3}>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={8}>模板名称:</Col>
            <Col span={16}>
              {
                addModal?<Input
                  type="textarea"
                  value={templateName2}
                  onChange={e => this.setState({ templateName2: e.target.value })}
                />:<Input
                  type="textarea"
                  value={templateName}
                  onChange={e => this.setState({ templateName: e.target.value })}
                />
              }
              
            </Col>
          </Row>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={8}>匹配脚本字段:</Col>
            <Col span={16}>
              <TextArea
                rows={2}
                value={matchingHandlerScript}
                onChange={e => this.setState({ matchingHandlerScript: e.target.value })}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={8}>废页脚本字段:</Col>
            <Col span={16}>
              <TextArea
                rows={2}
                value={wastePageHandlerScript}
                onChange={e => this.setState({ wastePageHandlerScript: e.target.value })}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={8}>最终处理函数(data,text):</Col>
            <Col span={16}>
              <TextArea
                rows={2}
                value={backHandlerScript}
                onChange={e => this.setState({ backHandlerScript: e.target.value })}
              />
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
}

export default LabelImage;
