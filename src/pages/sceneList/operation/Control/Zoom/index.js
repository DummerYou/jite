import React from 'react';
import { Input } from 'antd';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';
import { ref } from 'react.eval';
import './index.scss';

const maxZoomValue = 500;
const Zoom = props => {
    const {scale,updateScale}  = props

    const minus = () => {
        if (!scale) return false;
        if (scale) {
            if (scale - 25 <= 25) {
                updateScale(25);
                ref("imageView.setScale",25)
            } else {
                let num1 = Number(scale) - 25
                updateScale(num1);
                ref("imageView.setScale",num1)
            }
        }
    };
    const plus = () => {
        if (scale === maxZoomValue) return false;
        if (scale !== null) {
            if (scale + 25 >= maxZoomValue) {
                updateScale(maxZoomValue);
                ref("imageView.setScale",maxZoomValue)
            } else {
                let num2 = Number(scale) + 25
                updateScale(num2);
                ref("imageView.setScale",num2)
            }
        }
    };

    return (
        <div className='zoom'>
            <span onClick={minus} className='op-icon'>
                <MinusOutlined />
            </span>
            <span className='zoom-value'>
                <Input
                    className='zoom-input'
                    value={scale ? `${scale}%` : '0%'}
                    onChange={e => {
                        const pattern = /([1-9]?\d|500)$/;
                        const val = e.target.value.split('%')[0];
                        if (val === '') {
                            updateScale(0);
                        } else if (Number(val) >= maxZoomValue) {
                            updateScale(maxZoomValue);
                        } else if (pattern.test(val)) {
                            updateScale(Number(val));
                        }
                    }}
                />
            </span>
            <span onClick={plus} className='op-icon'>
                <PlusOutlined />
            </span>
        </div>
    );
};
export default Zoom;
