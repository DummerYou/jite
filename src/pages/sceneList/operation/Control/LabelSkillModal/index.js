import React, { useRef } from 'react';
import { Modal, Carousel, Button } from 'antd';

import './index.scss';

const LabelSkillModal = props => {
    const { onCancel } = props;
    const carouselRef = useRef(null);
    function toPrevious(){
        if (carouselRef.current) {
            const carousel = Object(carouselRef.current);
            if (carousel) {
                carousel.slick.slickPrev();
            }
        }
    }
    function toNext(){
        if (carouselRef.current) {
            const carousel = Object(carouselRef.current);
            if (carousel) {
                carousel.slick.slickNext();
            }
        }
    }
    return (
        <Modal
            width={800}
            title='标注技巧'
            visible={true}
            onCancel={onCancel}
            className='label-skill-modal'
            footer={null}
        >
            <Carousel ref={carouselRef}>
                <div key={1}>
                    <div className='error-example example-item'>
                        <div className='example-img'>
                        </div>
                        <div className='example-text'>
                            <p>错误</p>
                            <p>一个标注框不应该框两行文字</p>
                        </div>
                    </div>
                    <div className='incorrect-example example-item'>
                        <div className='example-img'>
                        </div>
                        <div className='example-text'>
                            <p>正确</p>
                            <p>一个标注框只能标一行文字，多行文字，需换行标注</p>
                        </div>
                    </div>
                </div>
                <div key={2}>
                    <div className='error-example example-item'>
                        <div className='example-img'>
                        </div>
                        <div className='example-text'>
                            <p>错误</p>
                            <p>第一个标的太松，第二个标的太紧</p>
                        </div>
                    </div>
                    <div className='incorrect-example example-item'>
                        <div className='example-img'>
                        </div>
                        <div className='example-text'>
                            <p>正确</p>
                            <p>标注框能正好框住文本内容，允许适当留一点空隙，但不能框太紧</p>
                        </div>
                    </div>
                </div>
            </Carousel>
            <div className='switch-btns'>
                <Button onClick={toPrevious}>上一个</Button>
                <Button type='primary' onClick={toNext} style={{ marginLeft: 24 }}>
                    下一个
                </Button>
            </div>
        </Modal>
    );
};
export default LabelSkillModal;
