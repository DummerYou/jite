import React, { useEffect, useState} from 'react';
import { Modal, Button, Tooltip } from 'antd';
import polygonIcon from '../image/polygon.svg';
import Constants from '../common/Constants';
import { RotateLeftOutlined, RotateRightOutlined, QuestionCircleOutlined} from '@ant-design/icons';
import { ref } from 'react.eval';

import './index.scss';
import Zoom from './Zoom';
import LabelSkillModal from './LabelSkillModal';

const ControlView = props => {
    const { afterSave, loading, currentTask, total, jumpTask,selectedTool,setSelectedTool,scale,updateScale, isChange, saveLabels, clearStates,isFirstPage,isLastPage,item,isCompletedLabel} = props;

    //url数据
    let type = 1;
    let cycleId = 1;
    const regionStore = {regions:"123"};

    const handleToolClick = (e, name) => {
        console.log(name)
        console.log("name=>>>>>>>>")
        if(name == "rect"){
            // this.selectedTool = 1
            setSelectedTool(0)
            ref("imageView.setType","0")
        }else if(name == "polygon"){
            setSelectedTool(1)
            ref("imageView.setType",1)
        }
    };

    const save = async cb => {
        const res = await regionStore.save({ cycleId, type });
        // FIXED ISSUE: ensure image model cleared before callback function
        if (res && res.status === '0') {
            cb();
        }
    };

    const toNext = () => {
        if (item.getImage().loading || !isCompletedLabel) {
            return false;
        }
        const cb = async () => {
            if (!isLastPage()) {
                afterSave();
                const nextIndex = currentTask + 1;
                jumpTask(nextIndex);
            } else {
                Modal.info({
                    title: '您已完成最后最后一张的标注！',
                    footer: null,
                    className: 'complete-label-modal',
                    content: (
                        <div className='complete-label-modal-btns'>
                            <Button style={{ marginRight: 10 }} onClick={toCycle}>
                                返回
                            </Button>
                            <Button
                                type='primary'
                                onClick={() => {
                                    Modal.destroyAll();
                                    //更新初始值
                                    const currentData = regionStore.getLabelData();
                                }}
                            >
                                继续查看标注
                            </Button>
                        </div>
                    ),
                });
            }
        };
        save(cb);
    };

    function toCycle() {
        Modal.destroyAll();
        item.getStore().clearStates();
    }

    function counteClockWise() {
        // const result = rotation - 90 < 0 ? 270 : rotation - 90;
        // setRotation(result);
    }

    function clockWise() {
        // const result = rotation + 90 > 360 ? 90 : rotation + 90;
        // setRotation(result);
    }


    function renderRotateIcon(available, children) {
        if (available) {
            return children;
        } else {
            return (
                <Tooltip title='已标注时，旋转无法使用' placement='bottom'>
                    {children}
                </Tooltip>
            );
        }
    }


    return (
        <div className='control-bar not-select'>
            <div className={loading ? 'control-bar-zoom not-click' : 'control-bar-zoom'}>
                <span className='zoom-desp'>图片缩放</span>
                <Zoom  
              scale={scale}
              updateScale={updateScale}/>
            </div>
           
            
           
        </div>
    );
};

export default ControlView;
