import React, { PureComponent } from 'react';
import {
  Pagination,
  Row,
  Col,
  Button,
  Input,
  Select,
  message,
  Icon,
  Upload,
  Space,
  Menu,
  Drawer,
  Dropdown,
  Modal,
} from 'antd';
import { connect } from 'dva';
import { PlusOutlined, EllipsisOutlined, UploadOutlined } from '@ant-design/icons';
import { ref } from 'react.eval';
import './style.scss';
import { uploadTicket } from '../../../api/index';
// components
// import SideBar from './SideBar';
import TaskManagement from './TaskManagement';
import ControlView from './Control';
import HtxImage from './ImageView';
import LabelImage from './LabelImage';
import Preview from './preview';
import ImageViewCanvas from "./ImageView/ImageViewCanvas"

const { Option } = Select;
const { Search } = Input;
const { TextArea } = Input;
const { confirm } = Modal;
@connect(({ sceneModel, loading }) => ({ ...sceneModel, loading }))
class ScenePageOperation extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      regionStore: {
        regions: [],
        currentRegionId: '',
        isChange: false,
      },
      id: '',
      recordsItem: {},
      recordsList: [],
      recordsKey: null,
      data: [],
      dataNum: [],
      imgDatas: [],
      imgLists: [],
      imgUrl: '/scene-center/file/image?fileName=',
      tasks: [],
      metas: [],
      scale: 100,
      loading2: false,
      selectedTool: 0,
      image: {
        type: 'image',
        regions: [],
        regionEditor: {
          type: 'regionEditor',
          context: '',
          visible: false,
        },
        _value: '',
        maxwidth: '750px',
        naturalWidth: 1,
        naturalHeight: 1,
        initialWidth: 1,
        initialHeight: 1,
        stageWidth: null,
        stageHeight: null,
        imageScale: 1,
        cursorPositionX: 0,
        cursorPositionY: 0,
        isLabeling: false,
        originLabelData: null,
        hiddenRegionId: null,
        intelligentRecongnitionLoading: false,
      },
      control: {
        type: 'control',
        currentPage: 0,
        rotation: null,
      },
      sideBar: {
        type: 'sideBar',
        currentMeta: null,
        isCompletedLabel: false,
        labelType: 'by-sort',
        initial: true,
      },
      pageSize: 10,
      currentTask: 0,
      total: 0,
      cycleId: '1',
      type: '2',
      labelNnpredictorStatus: null,
      imgList2: [],
      allList:[],
      matchingHandlerScript:"",
    };
    this.addForm = React.createRef();
    ref(this);
  }

  async componentDidMount() {
    const { dispatch } = this.props;
    const { imgUrl } = this.state;
    this.getTableList();
    console.log(this.props.location.search);
    let id = this.getQueryVariable('id');
    let fileId = this.getQueryVariable('fileId');
    try {
      const res11 = await dispatch({
        type: 'sceneModel/selectFileInfo',
        payload: {
          "current": 1,
          "size": 9999,
          sceneId: id, //页码图路径
        },
      });
      console.log(res11)
      let dataList = []
      let dataNum = []
      let fileListS = res11.data.records
      if (fileId) {
        if(fileId.split(",").length){
          fileListS = fileId.split(",").map(v=>{
            return { id: v }
          })
        }else{
          fileListS = [{ id: fileId }]
        }
      }
      console.log("object")
      for (var i = 0; i < fileListS.length; i++) {
        const reFileInfo = await dispatch({
          type: 'sceneModel/getFileInfo',
          payload: {
            id: fileListS[i].id
          },
        });
        dataList.push(reFileInfo.data)
      }
      console.log(dataList);
      let imgDatas = [].concat.apply(
        [],
        dataList.length &&
        dataList.map((v, i) => {
          v.imgList.forEach((vv, ii) => {
            dataNum.push(v.id)
          })
          return v.imgList;
        })
      );
      console.log(imgDatas);
      console.log(dataNum);
      this.setState({
        data: dataList,
        dataNum: dataNum,
        imgDatas: imgDatas,
        total: imgDatas.length,
      }, () => {
        this.getselectTemplateItem(0)
        this.getRecordsList();
      });
    } catch (error) {

    }

    this.setState({
      id: id,
    });
  }

  async getRecordsList(name) {
    const { dispatch } = this.props;
    const {dataNum,currentTask,recordsItem}=this.state;
    try {
      const res = await dispatch({
        type: 'sceneModel/selectTemplateInfo',
        payload: {
          current: 1,
          size: 99999999,
          fileId: dataNum[currentTask],
        },
      });
      if (res && res.code == 200) {
        if (res.data.records.length) {
          this.setState({ recordsList: res.data.records },()=>{
            console.log(recordsItem)
              setTimeout(() => {
                if(!recordsItem.id){
                  console.log("object...............................")
                  const {recordsKey}=this.state;
                  console.log(res.data.records)
                  console.log(recordsKey)
                  res.data.records.forEach((v) => {
                    if (v.id == recordsKey) {
                      this.setState({ recordsItem: v});
                    }
                  })
                }
              }, 200);
          });
          if(name){
            let lists = JSON.parse(JSON.stringify(res.data.records))
            lists.reverse().some(v => {
              if (v.name == name) {
                this.getRecordsItem(v.templateId,v);
                return true
              }
            })
          }
        }
      }

    } catch (e) {
      console.log(e);
    }
  }
  async getRecordsItem(ids,list) {
    const { dispatch } = this.props;
    const { recordsList } = this.state

    // let id = this.getQueryVariable('id');
    // console.log(id)
    let vv = {}
    if(list){
      vv = list
    }else if (ids) {
      recordsList.forEach((v) => {
        if (v.id == ids) {
          vv = v
        }
      })
    }
    console.log(ids)
    console.log(vv)
    console.log(vv.id)
    this.setState({ recordsItem: vv, recordsKey: vv.id || ids });
    // console.log(id)
    console.log(ids)
    console.log(vv)

    // try {
    //   const res = await dispatch({
    //     type: 'sceneModel/selectTemplateInfo',
    //     payload: {
    //       sceneId: id,
    //     },
    //   });
    //   console.log(res.data.records.length)
    //   console.log(res.data)
    //   console.log(res.data.records[0])
    //   if (res && res.code == 200) {
    //     if (res.data.records.length) {
    //     }
    //   }

    // } catch (e) { }
  }
  setrecordsItem(data,type) {
    const {recordsItem}=this.state
    if(type){
      console.log(data)
      if(data.matchingHandlerScript3){
        this.setState({
          matchingHandlerScript: data.matchingHandlerScript3,
        });
      }
      this.setState({
        recordsItem: Object.assign(recordsItem,data),
      },()=>{
        console.log(recordsItem)
      });
    }else{
      this.getRecordsList(data.name)
    }
  }
  async getselectTemplateItem(id) {
    const { dispatch } = this.props
    const { dataNum, imgDatas,allList } = this.state
    let state = false;
    let setAlls = null;
    console.log(allList)
    try {
      if(allList[id]){
        setAlls = JSON.parse(JSON.stringify(allList[id]));
      }else{
        const res = await dispatch({
          type: 'sceneModel/selectTemplateItem',
          payload: {
            fileId: dataNum[id],
          },
        });
        if (res.data && res.data.length) {
          res.data.reverse().some(v => {
            console.log(this.getdataNumIndex(v.fileId, v.imgIndex), id)
            if (this.getdataNumIndex(v.fileId, v.imgIndex) == id) {
              console.log("object+++++++++++++++++++++++++++++++++++++++++++")
              let newAllList = JSON.parse(JSON.stringify(allList));
              newAllList[id] = v;
              this.setState({
                allList:JSON.parse(JSON.stringify(newAllList))
              })
              setAlls = v;
              
              return true
            }
          })
          console.log("object-*************++++++++")
        }
      }
      console.log(setAlls)
      if(setAlls){
        console.log("v\\\\\\\\\\\\")
        console.log(setAlls)
        this.getRecordsItem(setAlls.templateId);
        let fieldList = setAlls.itemFieldList ? setAlls.itemFieldList.map(v => {
          v.tagId = v.tagId+""
          v.tagType = v.type+""
          try {
            let position = JSON.parse(v.position)
            let imgGets = new ImageViewCanvas();
            return imgGets.getStructure(position[0], position[1], position[2], position[3], v)
            
          } catch (error) { 
            console.log(error)
            return v
           }
        }):[]
        console.log(fieldList)
        this.setState({
          imgList2: JSON.parse(JSON.stringify(fieldList)),
          matchingHandlerScript:setAlls.matchingHandlerScript
        });
        this.setImgUrl(imgDatas[id], fieldList);
        state = true
        
      }else{
        this.setState({
          matchingHandlerScript:""
        });
      }
      if (!state) {
        this.setImgUrl(imgDatas[id]);
      }
    } catch (error) {
      console.log(error)
    }
  }
  getdataNumIndex(a, b) {
    const { dataNum } = this.state
    var bs = dataNum.map((item, index) => {
      return item = {
        x: index,
        y: item
      }
    })
    if (bs.filter(item => item.y == a)[b]) {
      return bs.filter(item => item.y == a)[b].x
    } else {
      return -1
    }
  }

  getQueryVariable(variable) {
    var query = this.props.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (pair[0] == variable) {
        return pair[1];
      }
    }
    return false;
  }
  setImgUrl(url, list) {
    const { imgUrl } = this.state;
    console.log(url);
    if (url) {
      ref('imageView.initialize', { url: ('http://10.104.1.142:8880' + imgUrl + url), list });
    }
  }
  
  saveTemplate = async( type,cd ) =>{
    const {imgList2, imgDatas, currentTask, dataNum ,recordsItem,jumpTask ,total,allList ,matchingHandlerScript} = this.state;
    const imgList =  JSON.parse(JSON.stringify(imgList2)) ;
    const {dispatch} = this.props;
    const _this = this;
    console.log("保存模板");
    let imgIndex = this.getdataNumIndex2(currentTask)
    let payload = {
      "imgIndex": imgIndex,//页码索引
      "itemFieldList": imgList,
      "templateId": recordsItem.id || '',	//模板id
      "fileId": dataNum[currentTask],	//文件id
      "name": recordsItem.name || '',	//名称
      "matchingHandlerScript": matchingHandlerScript || '',	//模板匹配逻辑脚本
    }
    let newAllList2 = JSON.parse(JSON.stringify(allList));
    try {
      payload = Object.assign(JSON.parse(JSON.stringify(newAllList2[currentTask])) ,payload)
    } catch (error) {}
    if(newAllList2[currentTask]){
      newAllList2.splice(currentTask,1,payload)
    }else{
      newAllList2[currentTask] = JSON.parse(JSON.stringify(payload));
    }
    this.setState({
      allList:JSON.parse(JSON.stringify(newAllList2)),
    },()=>{
      if(type){
        
        if(type == 999){
          if(cd){
            cd(newAllList2)
          }
        }else{
          confirm({
            title: '全局保存',
            okText: '确定',
            cancelText: '取消',
            onOk() {
              _this.saveTemplateAll()
            },
            onCancel() {
              console.log('Cancel');
            },
          });
        }

      }
    })

  }
  saveTemplateAll = async() =>{
    const {dispatch } = this.props;
    const { allList } = this.state;
    const _this = this;
    console.log("保存模板");
    let allListNew = JSON.parse(JSON.stringify(allList))
    try {
      console.log(allListNew)
      let state = false;
      let payload = [];
      let payload2 = allListNew.map(v=>{
        if(!v.templateId && (!v.itemFieldList || !v.itemFieldList.length)){
          return {stateTemplateId:true}
        }else{
          return {
            id:v.id,
            "imgIndex": v.imgIndex,//页码索引
            "itemFieldList":v.itemFieldList ? v.itemFieldList.map(vv=>{
              return {
                "id":vv.id,
                "name":vv.name,
                "tagId": vv.tagId,	//0.表头 1.表体 2.表尾
                "position": vv.x1?`[${vv.x1},${vv.y1},${vv.x2},${vv.y2}]`:vv.position,		//标记字段坐标
                "type": vv.tagType,	//类型，标记，需要imgPageNum和position参数
                "handlerScript": vv.handlerScript,	//处理脚本
                "value": vv.value,	//
              }
            }):[],
            "templateId": v.templateId,	//模板id
            "fileId": v.fileId,	//文件id
            "name": v.name,	//名称
            "matchingHandlerScript": v.matchingHandlerScript	//模板匹配逻辑脚本
          }
        }
      })
      try {
        payload = payload2.filter(v=>{
          return !v.stateTemplateId
        })
      } catch (error) {
        console.log(error)
      }
      const res = await dispatch({
        type: 'sceneModel/saveBatchTemplateItem',
        payload,
      });
      console.log(res);
      if(res.code == 200){
        confirm({
          title: '保存成功是否返回',
          okText: '确定',
          cancelText: '取消',
          onOk() {
            window.location.href = window.location.href.split('/operation')[0];
          },
          onCancel() {
            console.log('Cancel');
          },
        });
        
      }
    } catch (error) {
      console.log("object")
      error.message && message.error(error.message);
    }
  }
  getdataNumIndex2(v){
    const {dataNum} = this.state
    var bs=dataNum.map((item,index)=>{
        return item={
        x:index,
        y:item
    }
    })
    var k=bs[v]
    var m=bs.filter(item=>item.y==k.y)
    if(m.findIndex(item=>item.x==k.x) != -1){
      return m.findIndex(item=>item.x==k.x)
    }else{
      return false
    }
  }

  getTableList = async () => { };
  // 新增/修改场景
  sceneNew = async type => { };
  // 复制场景
  copySceneInfoClick = async id => { };
  // .删除场景
  deleteSceneInfoClick = async id => { };
  handleChange = e => { };
  onSearch = e => { };
  handleSearch = e => { };
  jumpTask = index => {
    const { imgDatas ,allList} = this.state;
    this.saveTemplate()
    console.log(index);
    this.setState({
      currentTask: index,
    },()=>{
      this.getRecordsList()
    });
    this.setImgList([])
    ref('labelImage.setThisId', null);
    this.getselectTemplateItem(index)
    
  };
  isFirstPage = () => {
    const { currentTask } = this.state;
    return currentTask <= 0;
  };
  isLastPage = () => {
    const { currentTask, total } = this.state;
    return currentTask >= total - 1;
  };
  setImgList = (v, type) => {
    const { imgList2 } = this.state;
    console.log(imgList2, v);
    let vv = v ? v : imgList2
    this.setState({
      imgList2: JSON.parse(JSON.stringify(vv)),
    });
    ref('imageView.setImgList2', vv);
  };

  setSelectedTool = e => {
    this.setState({
      selectedTool: e,
    });
  };
  updateScale = e => {
    this.setState({
      scale: e,
    });
  };
  render() {
    const {
      total,
      type,
      currentTask,
      sideBar,
      metas,
      control,
      clearStates,
      image,
      regionStore,
      loading2,
      selectedTool,
      scale,
      imgDatas,
      imgUrl,
      imgList2,
      dataNum,
      data,
      id,
      recordsItem,
      recordsList,
      recordsKey,
      allList,
      matchingHandlerScript,
    } = this.state;

    const { isFirstPage, isLastPage } = control;
    const { afterSave } = image;
    const { save, isChange } = regionStore;
    return (
      <div className="ScenePageOperation">
        <div>
          <TaskManagement
            total={total}
            saveLabels={save}
            loading={loading2}
            jumpTask={this.jumpTask}
            isChange={isChange}
            afterSave={afterSave}
            isLastPage={this.isLastPage}
            isFirstPage={this.isFirstPage}
            getRecordsItem={this.getRecordsItem.bind(this)}
            saveTemplate={this.saveTemplate.bind(this)}
            allList={allList}
            dataNum={dataNum}
            currentTask={currentTask}
            clearStates={clearStates}
            recordsList={recordsList}
            ids={id}
            recordsKey={recordsKey}
          />
        </div>
        <div className="label-tool-container">
          <ControlView
            item={control}
            total={total}
            saveLabels={save}
            loading={loading2}
            jumpTask={this.jumpTask}
            isChange={isChange}
            afterSave={afterSave}
            isLastPage={this.isLastPage}
            isFirstPage={this.isFirstPage}
            currentTask={currentTask}
            selectedTool={selectedTool}
            scale={scale}
            updateScale={this.updateScale}
            setSelectedTool={this.setSelectedTool}
            clearStates={clearStates}
          />
          <div className="label-tool">
            <div className="label-imgs">
              {imgDatas.length
                ? imgDatas.map((v, i) => (
                  <div
                    className={currentTask == i ? 'imgLists this' : 'imgLists'}
                    onClick={() => this.jumpTask(i)}
                  >
                    <img src={imgUrl + v} alt="" />
                    <p>{i + 1}</p>
                  </div>
                ))
                : null}
            </div>
            <div
              className="label-tool-main"
              style={{ width: type === 'locating' ? '100%' : 'calc(100% - 500px)' }}
            >
              <HtxImage id="imageView" />
            </div>
            <div className="label-item">
              <LabelImage id="labelImage" setImgList={this.setImgList} matchingHandlerScript2={matchingHandlerScript} recordsList={recordsList} recordsKey={recordsKey} getRecordsItem={this.getRecordsItem.bind(this)} imgList={imgList2} imgDatas={imgDatas} ids={id} currentTask={currentTask} dataNum={dataNum} recordsItem={recordsItem} total={total} jumpTask={this.jumpTask} setrecordsItem={this.setrecordsItem.bind(this)} />
            </div>
          </div>
        </div>
        <Preview id="preview" dataList={data} getdataNumIndex={this.getdataNumIndex.bind(this)} imgDatas={imgDatas} imgList={imgList2} allList={allList} saveTemplate={this.saveTemplate.bind(this)}></Preview>
      </div>
    );
  }
}

export default ScenePageOperation;
