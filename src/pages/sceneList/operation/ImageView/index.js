import React, { PureComponent } from 'react';
import {
  Pagination,
  Row,
  Col,
  Button,
  Input,
  Select,
  message,
  Icon,
  Upload,
  Space,
  Menu,
  Drawer,
  Dropdown,
  Spin,
} from 'antd';
import { connect } from 'dva';
import { ref } from 'react.eval';
import { PlusOutlined, EllipsisOutlined, UploadOutlined } from '@ant-design/icons';
import './index.scss';
import { uploadTicket } from '../../../../api/index';
import ImageViewCanvas from "./ImageViewCanvas"

const { Option } = Select;
const { Search } = Input;
const { TextArea } = Input;
@connect(({ sceneModel, loading }) => ({ ...sceneModel, loading }))
class ImageView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading3: false,
      canvas:null
    };
    this.addForm = React.createRef();
    ref(this);
  }
  componentDidMount() {
    this.setState({ canvas: new ImageViewCanvas({},"cavasDiv") });
  }
  setScale(e){
    const {canvas} = this.state
    canvas.setScale(e)
  }
  setImgList2(list){
    const {canvas} = this.state
    canvas.setImgList(list)
  }
  setCurrent(list){
    const {canvas} = this.state
    canvas.setCurrent(list)
  }
  // 初始化
  initialize(url){
    const {canvas} = this.state
    console.log(url);
    this.setState({
      imgUrl:url,
    },()=>{
      canvas.getLoading = (type) =>{
        console.log(type+"---------------")
        this.setState({
          loading3:type
        })
      }
      canvas.getCurrent = (current) =>{   
        ref('labelImage.setThisId', current.id);   
        ref('labelImage.showModal', current);
      }
      canvas.initialize(url);
    })
    
  }
  render() {
    const { loading3 } = this.state;
    console.log(loading3)
    return (
      <Spin spinning={loading3} tip="图片加载中..." wrapperClassName="ImageView">
        <div className="cavasDiv" id="cavasDiv">
          
        </div>
      </Spin>
    );
  }
}

export default ImageView;
