
class ImageViewCanvas {
  constructor(brand,id) {
    this.state = Object.assign({
      loading: true,
      clickState:1,//点击状态   直接点击触发0，需要有list触发1
      c: null,
      ctx: null,
      startx: null, //起始x坐标
      starty: null, //起始y坐标
      flag: null, //是否点击鼠标的标志
      x: null,
      y: null,
      leftDistance: null,
      topDistance: null,
      op: 0, //op操作类型 0 无操作 1 画矩形框 2 拖动矩形框
      scale: 1,//缩放比例
      type: 0,//类型  矩形0 多边形1
      currentR: null, //当前点击的矩形框
      currently: null,//当前选中的矩形框
      layers: [], //图层
      elementWidth: 0,
      elementHeight: 0,
      imgWidth:0,
      imgHeight:0,
      strokeStyle: 'rgba(255,0,0,1)',
      strokeStyle2: 'rgba(255,0,0,0.5)',
      sbWidth: 5,//操作宽度
      imgUrl:'',
    }, brand);
    if(id){
      try {
        var oDiv = document.createElement('canvas');
        let num = Math.floor(Math.random()*(1000000-100000+1)+100000)
        oDiv.id = 'imageViewCanvas' + num;
        document.getElementById(id).appendChild(oDiv);
        this.state.c = document.getElementById('imageViewCanvas' + num)
        this.setImg();
        console.log(this.state)
      } catch (error) {
        console.log(error)
      }
    }
  }
  getStructure(x1,y1,x2,y2,list){
    const {  strokeStyle ,strokeStyle2} = this.state;
    return Object.assign(list,this.fixPosition({
      x1,y1,x2,y2,
      strokeStyle: strokeStyle,
      strokeStyle2: strokeStyle2,
    }))

  }
  // 获取全部
  getList(e){
    console.log(this.state.layers)
    return this.state.layers
  }
  // 获取当前
  getCurrent(current){
    console.log(current)
    return current
  }
  getLoading(type){
    console.log(type)
  }
  // 设置当前
  setCurrent(current){
    const {
      ctx,
      elementWidth,
      elementHeight,
      x,
      y,
    } = this.state;
    console.log(current)
    // return current
    this.setCurrentlyItem(current);
    try {
      this.setState({ currentR: null, flag: 0 }, () => {
        ctx.clearRect(0, 0, elementWidth, elementHeight);
        this.reshow(x, y);
        this.setState({ op: 0 });
      });
    } catch (error) {
      
    }
  }
  // 设置缩放比例 500 - 0
  setScale(e){
    const {c, ctx,elementWidth,elementHeight ,imgWidth,imgHeight} = this.state;
    console.log("object")
    const scale = e/100
    this.setState({scale:scale},()=>{
      console.log(imgWidth,imgHeight,scale)
      c.width = imgWidth*scale;
      c.height = imgHeight*scale;
      // c.style.backgroundImage = 'url(' + img.src + ')';
      c.style.backgroundSize = `${imgWidth*scale}px ${imgHeight*scale}px`;
      // 画布上绘图的环境
      this.setState({
        ctx: c.getContext('2d'),
        elementWidth: imgWidth*scale,
        elementHeight: imgHeight*scale,
      });
      ctx.clearRect(0, 0, elementWidth, elementHeight);
      this.reshow(0, 0)
    });
  }
  // 覆盖数组 如果需要返回操作需要
  setImgList(list){
    const {c, ctx,elementWidth,elementHeight ,imgWidth,imgHeight,x,y,layers} = this.state;
    try {
      ctx.clearRect(0, 0, elementWidth, elementHeight);
      ctx.restore();
      this.setState({
        layers:JSON.parse(JSON.stringify(list))
      },()=>{
        this.reshow(x, y);
      })
    } catch (error) {
      
    }
    
  }





  setType(e){
    this.setState({type:e});
    console.log(e)
  }








  // 初始化
  initialize(obj){
    console.log(obj)
    this.getLoading(true)
    this.setState({
      loading: true,
      ctx: null,
      startx: null, //起始x坐标
      starty: null, //起始y坐标
      flag: null, //是否点击鼠标的标志
      x: null,
      y: null,
      leftDistance: null,
      topDistance: null,
      op: 0, //op操作类型 0 无操作 1 画矩形框 2 拖动矩形框
      currentR: null, //当前点击的矩形框
      currently: null,//当前选中的矩形框
      layers: obj.list||[], //图层
      elementWidth: 0,
      elementHeight: 0,
      imgUrl:obj.url,
    },()=>this.setImg())
    
  }
  setImg() {
    const _this = this;
    const { c,scale,imgUrl,layers } = this.state;
    console.log(this.state)
    // 设置图片
    const img = new Image();
    img.src = imgUrl;
    console.log(imgUrl)
    console.log(img.src);
    img.onload = function () {
      // 图片加载完成进行操作
      _this.getLoading(false)
      _this.setState({ loading: false,imgWidth:this.width,imgHeight:this.height });
      console.log(this.width);
      console.log(this.height);
      console.log("img.src");

      // 设置canvas宽高显示的图片
      c.width = this.width*scale;
      c.height = this.height*scale;
      c.style.backgroundImage = 'url(' + img.src + ')';
      c.style.backgroundSize = `${this.width*scale}px ${this.height*scale}px`;
      
      // 画布上绘图的环境
      _this.setState({
        ctx: c.getContext('2d'),
        elementWidth: this.width*scale,
        elementHeight: this.height*scale,
      },()=>{
        if(layers && layers.length){
          _this.reshow(0, 0)
        }
      });
      c.onmouseleave = function () {
        c.onmousedown = null;
        c.onmousemove = null;
        c.onmouseup = null;
      };
      c.onmouseenter = function () {
        c.onmousedown = _this.mousedown.bind(_this);
        c.onmousemove = _this.mousemove.bind(_this);
        document.onmouseup = _this.mouseup.bind(_this);
      };
    };
  }
  // 设置鼠标样式
  resizeLeft(rect) {
    const { c, flag, op, currentR, x, y } = this.state;
    c.style.cursor = 'w-resize';
    if (flag && op == 0) {
      this.setState({ op: 3 });
    }
    let currentR1 = currentR;
    if (flag && (op == 3 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.x1 = x;
      currentR1.width = currentR1.x2 - currentR1.x1;
    }
    console.log(op);
    this.setState({ currentR: currentR1 });
  }
  resizeTop(rect) {
    const { c, flag, op, currentR, x, y } = this.state;
    c.style.cursor = 's-resize';
    if (flag && op == 0) {
      this.setState({ op: 4 });
    }
    let currentR1 = currentR;
    if (flag && (op == 4 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.y1 = y;
      currentR1.height = currentR1.y2 - currentR1.y1;
    }
    this.setState({ currentR: currentR1 });
  }
  resizeWidth(rect) {
    const { c, flag, op, currentR, x, y } = this.state;
    c.style.cursor = 'w-resize';
    if (flag && op == 0) {
      this.setState({ op: 5 });
    }
    let currentR1 = currentR;
    if (flag && (op == 5 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.x2 = x;
      currentR1.width = currentR1.x2 - currentR1.x1;
    }
    this.setState({ currentR: currentR1 });
  }
  resizeHeight(rect) {
    const { c, flag, op, currentR, x, y } = this.state;
    c.style.cursor = 's-resize';
    if (flag && op == 0) {
      this.setState({ op: 6 });
    }
    let currentR1 = currentR;
    if (flag && (op == 6 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.y2 = y;
      currentR1.height = currentR1.y2 - currentR1.y1;
    }
    this.setState({ currentR: currentR1 });
  }
  resizeLT(rect) {
    const { c, flag, op, currentR, x, y } = this.state;
    c.style.cursor = 'se-resize';
    if (flag && op == 0) {
      this.setState({ op: 7 });
    }
    let currentR1 = currentR;
    if (flag && (op == 7 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.x1 = x;
      currentR1.y1 = y;
      currentR1.height = currentR1.y2 - currentR1.y1;
      currentR1.width = currentR1.x2 - currentR1.x1;
    }
    this.setState({ currentR: currentR1 });
  }
  resizeWH(rect) {
    const { c, flag, op, currentR, x, y } = this.state;
    c.style.cursor = 'se-resize';
    if (flag && op == 0) {
      this.setState({ op: 8 });
    }
    let currentR1 = currentR;
    if (flag && (op == 8 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.x2 = x;
      currentR1.y2 = y;
      currentR1.height = currentR1.y2 - currentR1.y1;
      currentR1.width = currentR1.x2 - currentR1.x1;
    }
    this.setState({ currentR: currentR1 });
  }
  resizeLH(rect) {
    const { c, flag, op, currentR, x, y } = this.state;
    c.style.cursor = 'ne-resize';
    if (flag && op == 0) {
      this.setState({ op: 9 });
    }
    let currentR1 = currentR;
    if (flag && (op == 9 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.x1 = x;
      currentR1.y2 = y;
      currentR1.height = currentR1.y2 - currentR1.y1;
      currentR1.width = currentR1.x2 - currentR1.x1;
    }
    this.setState({ currentR: currentR1 });
  }
  resizeWT(rect) {
    const { c, flag, op, currentR, x, y } = this.state;
    c.style.cursor = 'ne-resize';
    if (flag && op == 0) {
      this.setState({ op: 10 });
    }
    let currentR1 = currentR;
    if (flag && (op == 10 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.x2 = x;
      currentR1.y1 = y;
      currentR1.height = currentR1.y2 - currentR1.y1;
      currentR1.width = currentR1.x2 - currentR1.x1;
    }
    this.setState({ currentR: currentR1 });
  }
  setArc(x, y) {
    const { ctx, sbWidth,scale } = this.state;
    ctx.beginPath();
    ctx.arc(x*scale, y*scale, sbWidth, 0, 2 * Math.PI);
    ctx.fillStyle = '#fff';
    ctx.fill();
    ctx.stroke();
  }
  setCurrently1() {
    const { currently } = this.state;
    if (currently) {
      let x3 = (currently.x2 - currently.x1) / 2 + currently.x1;
      let y3 = (currently.y2 - currently.y1) / 2 + currently.y1;
      this.setArc(currently.x1, currently.y1);
      this.setArc(currently.x2, currently.y2);
      this.setArc(currently.x1, currently.y2);
      this.setArc(currently.x2, currently.y1);

      this.setArc(x3, currently.y1);
      this.setArc(x3, currently.y2);
      this.setArc(currently.x1, y3);
      this.setArc(currently.x2, y3);
    }
  }
  // 画图
  reshow(x, y) {
    const { flag, ctx, op, layers, scale, sbWidth, currently } = this.state;
    // console.log(flag);
    let allNotIn = 1;
    let resize = 0;
    layers.forEach(item => {
      if (item.type == 0) {
        ctx.beginPath();
        ctx.rect(item.x1*scale, item.y1*scale, item.width*scale, item.height*scale);
        ctx.strokeStyle = item.strokeStyle;
        ctx.fillStyle = item.strokeStyle2;
        ctx.fill();
        // console.log(x,y,item.x1,item.x2,item.y1,item.y2);
        if (
          (x >= item.x1 - sbWidth / scale &&
            x <= item.x2 + sbWidth / scale &&
            y >= item.y1 - sbWidth / scale &&
            y <= item.y2 + sbWidth / scale) ||
          op > 1
        ) {
          let x3 = (item.x2 - item.x1) / 2 + item.x1;
          let y3 = (item.y2 - item.y1) / 2 + item.y1;
          if (
            (x >= item.x1 - sbWidth / scale &&
              x <= item.x1 + sbWidth / scale &&
              y >= y3 - sbWidth / scale &&
              y <= y3 + sbWidth / scale) ||
            op == 3
          ) {
            resize = 1;
            this.resizeLeft(item);
          } else if (
            (x >= item.x2 - sbWidth / scale &&
              x <= item.x2 + sbWidth / scale &&
              y >= y3 - sbWidth / scale &&
              y <= y3 + sbWidth / scale) ||
            op == 5
          ) {
            resize = 1;
            this.resizeWidth(item);
          } else if (
            (x >= x3 - sbWidth / scale &&
              x <= x3 + sbWidth / scale &&
              y >= item.y1 - sbWidth / scale &&
              y <= item.y1 + sbWidth / scale) ||
            op == 4
          ) {
            resize = 1;
            this.resizeTop(item);
          } else if (
            (x >= x3 - sbWidth / scale &&
              x <= x3 + sbWidth / scale &&
              y >= item.y2 - sbWidth / scale &&
              y <= item.y2 + sbWidth / scale) ||
            op == 6
          ) {
            resize = 1;
            this.resizeHeight(item);
          } else if (
            (x >= item.x1 - sbWidth / scale &&
              x <= item.x1 + sbWidth / scale &&
              y >= item.y1 - sbWidth / scale &&
              y <= item.y1 + sbWidth / scale) ||
            op == 7
          ) {
            resize = 1;
            this.resizeLT(item);
          } else if (
            (x >= item.x2 - sbWidth / scale &&
              x <= item.x2 + sbWidth / scale &&
              y >= item.y2 - sbWidth / scale &&
              y <= item.y2 + sbWidth / scale) ||
            op == 8
          ) {
            resize = 1;
            this.resizeWH(item);
          } else if (
            (x >= item.x1 - sbWidth / scale &&
              x <= item.x1 + sbWidth / scale &&
              y >= item.y2 - sbWidth / scale &&
              y <= item.y2 + sbWidth / scale) ||
            op == 9
          ) {
            resize = 1;
            this.resizeLH(item);
          } else if (
            (x >= item.x2 - sbWidth / scale &&
              x <= item.x2 + sbWidth / scale &&
              y >= item.y1 - sbWidth / scale &&
              y <= item.y1 + sbWidth / scale) ||
            op == 10
          ) {
            resize = 1;
            this.resizeWT(item);
          } else {
            if (ctx.isPointInPath(x * scale, y * scale)) {
              this.renderss(item);
              allNotIn = 0;
            }
          }
        }

        ctx.stroke();
      }
    });
    this.setCurrently1();
    // console.log(flag,allNotIn,op);
    // console.log('op', op);
    if (flag && allNotIn && op < 3 && !resize) {
      this.setState({ op: 1 });
    }
  }
  // 移动
  renderss(rect) {
    const { flag, c, op, currentR, x, y, leftDistance, topDistance } = this.state;
    c.style.cursor = 'move';
    if (flag && op == 0) {
      this.setState({ op: 2 });
    }
    let currentR1 = currentR;
    if (flag && (op == 2 || op == 0)) {
      if (!currentR1) {
        currentR1 = rect;
      }
      currentR1.x2 += x - leftDistance - currentR1.x1;
      currentR1.x1 += x - leftDistance - currentR1.x1;
      currentR1.y2 += y - topDistance - currentR1.y1;
      currentR1.y1 += y - topDistance - currentR1.y1;
    }
    this.setState({ currentR: currentR1 });
  }
  // 判断是否有
  isPointInRetc(x, y) {
    const { layers } = this.state;
    let len = layers.length;
    for (let i = 0; i < len; i++) {
      if (
        layers[i].x1 < x + 2 &&
        x - 2 < layers[i].x2 &&
        layers[i].y1 < y + 2 &&
        y - 2 < layers[i].y2
      ) {
        return layers[i];
      }
    }
  }
  // 计算矩形
  fixPosition(position) {
    if (position.x1 > position.x2) {
      let x = position.x1;
      position.x1 = position.x2;
      position.x2 = x;
    }
    if (position.y1 > position.y2) {
      let y = position.y1;
      position.y1 = position.y2;
      position.y2 = y;
    }
    position.width = position.x2 - position.x1;
    position.height = position.y2 - position.y1;
    if (position.width < 10 || position.height < 10) {
      return null;
    }
    return position;
  }

  mousedown(e) {
    const { ctx, x, y, c, scale, strokeStyle } = this.state;
    let startx = (e.pageX - c.getBoundingClientRect().left) / scale;
    let starty = (e.pageY - c.getBoundingClientRect().top) / scale;
    let currentR1 = this.isPointInRetc(startx, starty);
    this.setState({ startx, starty, currentR: currentR1 });
    console.log(startx, starty, currentR1);
    if (currentR1) {
      this.setState({ leftDistance: startx - currentR1.x1, topDistance: starty - currentR1.y1 });
    }
    ctx.strokeRect(x, y, 0, 0);
    ctx.strokeStyle = strokeStyle;
    this.setState({ flag: 1 });
  }
  mousemove(e) {
    const {
      flag,
      c,
      ctx,
      op,
      currentR,
      leftDistance,
      topDistance,
      scale,
      elementWidth,
      elementHeight,
      startx,
      starty,
    } = this.state;
    let x = (e.pageX - c.getBoundingClientRect().left) / scale;
    let y = (e.pageY - c.getBoundingClientRect().top) / scale;
    this.setState({ x, y });
    ctx.save();
    ctx.setLineDash([5]);
    c.style.cursor = 'default';
    ctx.clearRect(0, 0, elementWidth, elementHeight);
    if (flag && op == 1) {
      ctx.strokeRect(startx*scale, starty*scale, x*scale - startx*scale, y*scale - starty*scale);
    }
    ctx.restore();
    this.reshow(x, y);
  }
  mouseup(e) {
    const {
      ctx,
      elementWidth,
      elementHeight,
      flag,
      c,
      op,
      currentR,
      x,
      y,
      leftDistance,
      topDistance,
      startx,
      starty,
      type,
      layers,
      strokeStyle,
      strokeStyle2,
      currently,
      clickState,
    } = this.state;
    if (op == 1) {
      let layersfix = layers;
      if(clickState == 0){
        let layersfixList = this.fixPosition({
          x1: startx,
          y1: starty,
          x2: x,
          y2: y,
          strokeStyle: strokeStyle,
          strokeStyle2: strokeStyle2,
          type: type,
          id:Math.floor(Math.random()*(1000000-100000+1)+100000),
        });
        if (layersfixList) {
          layersfix.push(layersfixList);
        }
        this.outDivItem(layersfixList);
      }else if(clickState == 1){
        if(currently){
          let layersfixList = this.fixPosition(Object.assign(currently,{
            x1: startx,
            y1: starty,
            x2: x,
            y2: y,
            strokeStyle: strokeStyle,
            strokeStyle2: strokeStyle2,
            type: type,
          }));
          if (layersfixList) {
            layersfix.push(layersfixList);
          }
          this.outDivItem(layersfixList);
        }
      }
      
      this.setState({ layers: layersfix });
    } else if (op >= 3) {
      this.outDivItem(currentR, true);
      this.fixPosition(currentR);
    } else {
      this.outDivItem(currentR, true);
    }
    try {
      this.setState({ currentR: null, flag: 0 }, () => {
        ctx.clearRect(0, 0, elementWidth, elementHeight);
        this.reshow(x, y);
        this.setState({ op: 0 });
      });
    } catch (error) {
      
    }
  }
  outDivItem(current, type) {
    console.log(current, type);
    this.setCurrentlyItem(current);
    if(current){
      // ref('labelImage.showModal', current);
      this.getCurrent(current)
    }
  }
  setCurrentlyItem(current) {
    this.setState({ currently: current });
  }
  setState(obj,cd){
    this.state = Object.assign(this.state,obj)
    if(cd){
      cd()
    }
  }

}

export default ImageViewCanvas;