import React, { PureComponent } from 'react';
import {
  Pagination,
  Row,
  Col,
  Button,
  Input,
  Select,
  message,
  Icon,
  Upload,
  Space,
  Menu,
  Drawer,
  Dropdown,
  Modal,
  Table,
  Progress,
  Spin,
} from 'antd';
import { connect } from 'dva';
import {
  PlusOutlined,
  EllipsisOutlined,
  UploadOutlined,
  InboxOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import './style.scss';
import { uploadTicket } from '../../api/scene';
const { Dragger } = Upload;
const { confirm } = Modal;

const { Option } = Select;
const { Search } = Input;
const { TextArea } = Input;
@connect(({ sceneModel, loading }) => ({ ...sceneModel, loading }))
class SceneList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      ModalVisible: false,
      ModalVisibleId: '',
      ModalVisibleloading: false,
      fileList: [],
      selectedRowKeys: [],
      selectVal: '',
      searchVal: '',
      folderId: '',
      headers: [],
      tableList: [],
      visible: false,
      upState: {
        9: 0,
      },
      CardList: [
        { type: 1 },
        { type: 2 },
        { type: 3 },
        { type: 4 },
        { type: 5 },
        { type: 6 },
        { type: 7 },
        { type: 8 },
        { type: 9 },
      ],
      textAreaVal: '',
      pagination: { current: 1, total: 0, pageSize: 14 }, // 分页
    };
    this.addForm = React.createRef();
  }

  componentDidMount() {
    const { pagination } = this.state;
    
    console.log(document.body.clientWidth)
    if(document.body.clientWidth){
      let paginationNew = pagination
      if(document.body.clientWidth<1420){
        paginationNew.pageSize = 8
      }else if(document.body.clientWidth<1800){
        paginationNew.pageSize = 11
      }
      this.setState({
        pagination:paginationNew,
      });
    }
    this.getTableList();
    // // this.getTableList();
    // this.getInvoiceType();
    // this.getGroupTree();
  }

  getTableList = async (e) => {
    const { dispatch } = this.props;
    console.log(e)
    try {
      const { pagination, searchVal, selectVal } = this.state;
      console.log(pagination, searchVal, selectVal);
      const res = await dispatch({
        type: 'sceneModel/getselectSceneInfo',
        payload: {
          size:  pagination.pageSize,
          current: e || pagination.current,
          name: searchVal,
          status: selectVal,
        },
      });
      if (!res) return;
      let { records, size, total, current } = res;
      if (records.length) {
        records.unshift({ status: -1 });
      } else {
        records = [{ status: -1 }];
      }
      this.setState({
        tableList: records || [],
        pagination: { pageSize: size, current: current, total:total },
      });
    } catch (error) {
      error.message && message.error(error.message);
    }
  };
  // 新增/修改场景
  sceneNew = async (type, list) => {
    const { dispatch } = this.props;
    console.log(type, list);
    try {
      const { textAreaVal } = this.state;
      let payload;
      if (type == 1) {
        // 新增
        payload = {
          name: textAreaVal,
        };
      } else if (type == 2) {
        // 修改
      } else if (type == 3) {
        // 上线
        payload = {
          id: list.id,
          status: 2,
        };
      } else if (type == 4) {
        // 下线
        payload = {
          id: list.id,
          status: 1,
        };
      }
      console.log(payload);
      const res = await dispatch({
        type: 'sceneModel/saveSceneInfo',
        payload,
      });
      if (res.code == 200) {
        this.setState({ visible: false });
        this.getTableList();
      }
    } catch (error) {
      error.message && message.error(error.message);
    }
  };
  // 复制场景
  copySceneInfoClick = async id => {
    const { dispatch } = this.props;
    try {
      const res = await dispatch({
        type: 'sceneModel/copySceneInfo',
        payload: {
          id,
        },
      });
      console.log(res);
      if (res.code == 200) {
        this.getTableList();
      }
    } catch (error) {
      console.log(error)
      error.message && message.error(error.message);
    }
  };
  // .删除场景
  deleteSceneInfoClick = async id => {
    const { dispatch } = this.props;
    try {
      const res = await dispatch({
        type: 'sceneModel/deleteSceneInfo',
        payload: {
          id,
        },
      });
      console.log(res);
      if (res.code == 200) {
        this.getTableList();
      }
    } catch (error) {
      error.message && message.error(error.message);
    }
  };
  handleChange = e => {
    console.log(e);
    this.setState({ selectVal: e });
  };
  onSearch = e => {
    console.log(e);
    this.setState({ searchVal: e });
    this.getTableList();
  };
  handleSearch = e => {
    console.log(e);
    this.setState({ searchVal: e.target.value });
  };

  onSelectChange = selectedRowKeys => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };
  openModal(id) {
    this.setState({
      ModalVisible: true,
      ModalVisibleId: id,
      fileList: [],
      selectedRowKeys: [],
    });
  }
  render() {
    const {
      CardList,
      ModalVisible,
      selectedRowKeys,
      fileList,
      visible,
      textAreaVal,
      pagination,
      tableList,
      upState,
      ModalVisibleloading,
    } = this.state;

    const fromCenNo = (
      <div className="fromCenNo">
        暂无模板数据，请<span className="brand">导入数据</span>，构建模板
        <br />
        支持zip,pdf,jpg,jpeg,png,bmp格式的数据直接导入或相应zip格式导入
      </div>
    );
    const props = {
      onRemove: file => {
        this.setState(state => {
          const index = state.fileList.indexOf(file);
          const newFileList = state.fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: file => {
        this.setState(state => ({
          fileList: [...state.fileList, file],
          selectedRowKeys: [...state.selectedRowKeys, file.uid],
        }));
        console.log(file);

        // this.setState({ selectedRowKeys });
        return false;
      },
      onChange: info => {
        console.log(info);
        const event = info.event;
        console.log(event);
      },
      fileList,
    };
    const columnsTable = [
      {
        title: '数据集名称',
        dataIndex: 'name',
      },
    ];
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };

    const showDrawer = () => {
      this.setState({ visible: true, textAreaVal: '' });
    };
    const onClose = () => {
      this.setState({ visible: false });
    };
    const onChange = e => {
      console.log('Change:', e.target.value);
      this.setState({ textAreaVal: e.target.value });
    };
    const drawerTop = (
      <Drawer
        className="scenedrawerDiv"
        placement="top"
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <h2>新建场景</h2>
        <p>场景名称</p>
        <TextArea value={textAreaVal} showCount maxLength={30} onChange={onChange} />
        <Space className="drawerBtn">
          <Button onClick={onClose}>取消</Button>
          <Button type="primary" onClick={() => this.sceneNew(1)}>
            新建场景
          </Button>
        </Space>
      </Drawer>
    );

    const menuItemClick = v => {
      console.log(v.item.props.list);
      try {
        const list = v.item.props.list;
        console.log(v.key);
        clicks(v.key, list);
      } catch (error) {
        error.message && message.error(error.message);
      }
    };
    const clicks = (key, list) => {
      let _this = this
      console.log(key, list);
      switch (key) {
        case '1':
          console.log('导入');
          this.openModal(list.id);
          break;
        case '2':
          // this.copySceneInfoClick(list.id)
          // console.log('模板');
          // console.log(window.location.href);
          window.location.href = window.location.href + '/fileList?id='+list.id;
          break;
        case '3':
          confirm({
            title: '应用是否上线',
            icon: <ExclamationCircleOutlined />,
            okText: '确定',
            cancelText: '取消',
            onOk() {
              _this.sceneNew(3, list);
              console.log('上线');
            },
            onCancel() {
              console.log('Cancel');
            },
          });
          break;
        case '4':
          this.copySceneInfoClick(list.id);
          break;
        case '5':
          this.deleteSceneInfoClick(list.id);
          break;
        case '6':
          window.location.href = window.location.href + '/operation?id='+list.id;
          
          break;
        case '7':
          confirm({
            title: '应用是否下线',
            icon: <ExclamationCircleOutlined />,
            okText: '确定',
            cancelText: '取消',
            onOk() {
              _this.sceneNew(4, list);
              console.log('下线');
            },
            onCancel() {
              console.log('Cancel');
            },
          });
          break;
        default:
          console.log(key);
      }
    };
    const menuList = v => {
      console.log(v)
      let type = v.status;
      // if(v.status == 1){
      //   type = 2
      // }
      return (
        <Menu onClick={menuItemClick}>
          <Menu.Item key="2" list={v}>
            文件列表
          </Menu.Item>

          {/* {type == 1 ? (
            <Menu.Item key="6" list={v}>
            定义模板
          </Menu.Item>
          ) : null} */}
          {type == 1 ? (
            <Menu.Item key="3" list={v}>
              应用上线
            </Menu.Item>
          ) : null}
          {type == 2 ? (
            <Menu.Item key="7" list={v}>
              应用下线
            </Menu.Item>
          ) : null}
          {type != 0 ? (
            <Menu.Item key="4" list={v}>
              复制场景
            </Menu.Item>
          ) : null}
          {type != 2 ? (
            <Menu.Item key="5" list={v}>
              删除场景
            </Menu.Item>
          ) : null}
        </Menu>
      );
    };
    const moduleDivs = v => {
      let id = v.id;
      let type = v.status;
      console.log(upState);
      console.log(id);
      if (type == -1) {
        return (
          <div className="addDiv" onClick={showDrawer}>
            <PlusOutlined />
            <p>点击新建一个场景</p>
          </div>
        );
      } else if (type >= 0) {
        return (
          <div className="fromDiv">
            <div className="fromTop">
              <span className="fromName text-ellipsis">{v.name}</span>
              {type == 0 ? (
                <span className="fromState warning">等待导入数据</span>
              ) : type == 1 ? (
                <span className="fromState success">等待上线</span>
              ) : type == 2 ? (
                <span className="fromState brand">服务中</span>
              ) : (
                <span className="fromState purple">等待定义模板</span>
              )}
            </div>
            <div className="fromCen">
              {type == 0 ? (
                upState[id] >= 0 ? (
                  <div className="fromUpload fromUpload1">
                    <p>正在导入的图像</p>
                    <Progress percent={upState[id]} />
                    {upState[id] > 0 ? (
                      <Button
                        type="link"
                        onClick={() => {
                          let newUpState = upState;
                          newUpState[id] = -1;
                          this.openModal(id);
                          this.setState({ upState: newUpState });
                        }}
                      >
                        取消导入
                      </Button>
                    ) : (
                      <Button
                        type="link"
                        onClick={() => {
                          let newUpState = upState;
                          newUpState[id] = -1;
                          this.openModal(id);
                          this.setState({ upState: newUpState });
                        }}
                      >
                        重新导入
                      </Button>
                    )}
                  </div>
                ) : (
                  fromCenNo
                )
              ) : type == 1 ? (
                <div className="fromUpload">
                  <p>为效果助力，可进行场景调优</p>
                </div>
              ) : type == 2 ? (
                <div className="fromUpload">
                  <p>模板准确，助力效果更优</p>
                </div>
              ) : (
                <div className="fromUpload">
                  <p>为效果助力，可继续标注</p>
                </div>
              )}
            </div>
            <Space className="fromBottom">
              <Dropdown overlay={menuList(v)} placement="bottomCenter">
                <Button icon={<EllipsisOutlined />} />
              </Dropdown>
              <Button type="primary" onClick={() => clicks('1', v)}>
                  导入数据
                </Button>
               {/* : type <= 7 ? (
                <Button type="primary">定义模板</Button>
              ) : type <= 8 ? (
                <Button type="primary" onClick={() => clicks('3', v)}>
                  应用上线
                </Button>
              )  */}
            </Space>
          </div>
        );
      }
    };

    const handleOk = () => {
      // this.setState({ ModalVisible: false });
      console.log('ok');
      handleUpload();
    };

    const handleCancel = () => {
      const { ModalVisibleloading, upState, ModalVisibleId } = this.state;
      const _this = this;
      if (ModalVisibleloading) {
        confirm({
          title: '导入中',
          icon: <ExclamationCircleOutlined />,
          content: '是否取消导入',
          okText: '确定',
          cancelText: '关闭',
          onOk() {
            let newUpState = upState;
            newUpState[ModalVisibleId] = -1;
            _this.setState({
              ModalVisible: false,
              upState: newUpState,
              ModalVisibleloading: false,
            });
          },
          onCancel() {
            console.log('Cancel');
          },
        });
      } else {
        this.setState({ ModalVisible: false });
      }
    };
    const handleUpload = async () => {
      const { fileList, upState, ModalVisibleId } = this.state;
      const formData = new FormData();
      const id = ModalVisibleId;
      formData.append('sceneId', id);
      if (!selectedRowKeys.length) {
        message.error('请选择文件');
        return;
      }
      let filesList = [];

      selectedRowKeys.forEach(v => {
        fileList.forEach(file => {
          if (v === file.uid) {
            console.log(v, file);
            // filesList.push(file)
            formData.append('files', file);
          }
        });
      });
      // formData.append('files', filesList);
      // [
      //   {
      //     "filePath": "/template/dde584bbe46244b1addc0d47437b89e4",
      //     "pdfFileName": "/template/dde584bbe46244b1addc0d47437b89e4.pdf",
      //     "imgDirectory": "/template/dde584bbe46244b1addc0d47437b89e4/",
      //     "imgNum": 5,
      //     "imgList": [
      //       "/template/dde584bbe46244b1addc0d47437b89e4/0.png",
      //       "/template/dde584bbe46244b1addc0d47437b89e4/1.png",
      //       "/template/dde584bbe46244b1addc0d47437b89e4/2.png",
      //       "/template/dde584bbe46244b1addc0d47437b89e4/3.png",
      //       "/template/dde584bbe46244b1addc0d47437b89e4/4.png"
      //     ]
      //   }
      // ]
      console.log(formData);
      try {
        console.log('1');
        this.setState({ ModalVisibleloading: true });
        const { data, code, message } = await uploadTicket({ body: formData });
        this.setState({ ModalVisibleloading: false });
        if (upState[id] == -1) {
          return;
        }

        console.log('2');
        console.log(data, code);
        if (code == 200) {
          console.log('上传成功');
          localStorage.setItem('sceneOperationData',JSON.stringify(data));
          let fileIds = "";
          try {
            fileIds = data.map(v=>{return v.id}).join()
          } catch (error) {
            console.log(error)
          }
          window.location.href = window.location.href + '/operation?id=' + id + "&fileId="+fileIds;

          // this.setState({ ModalVisible: false });
        } else {
          message.error(message);
        }
      } catch (error) {
        console.log('3');
        error.message && message.error(error.message);
      }

      console.log(fileList);
      console.log(selectedRowKeys);

      // this.setState({
      // });

      // this.setState({ ModalVisible: false });
    };

    return (
      <div className="sceneList">
        <div className="sceneSearch">
          <Select defaultValue="" style={{ width: 120 }} onChange={this.handleChange}>
            <Option value="">全部</Option>
            <Option value="0">待导入</Option>
            <Option value="1">待上线</Option>
            <Option value="2">服务中</Option>
          </Select>
          <Search
            placeholder="请输入场景名称或关键字"
            onChange={this.handleSearch}
            onSearch={this.onSearch}
            style={{ width: 220 }}
          />
        </div>
        <div className="sceneMain">
          <div className="projectsList">
            {tableList
              ? tableList.map(v => {
                  return (
                    <div className="projectWrapper">
                      <div className="projectPaddingArea left"></div>
                      <div className="projectCardWrapper">
                        <div className="popoverContainer">{moduleDivs(v)}</div>
                      </div>
                      <div className="projectPaddingArea right"></div>
                    </div>
                  );
                })
              : null}
          </div>
        </div>

        <Modal
          title="导入图像"
          visible={ModalVisible}
          okText="立即导入"
          onOk={handleOk}
          onCancel={handleCancel}
          confirmLoading={ModalVisibleloading}
        >
          <Dragger {...props} showUploadList={false}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">请将图像压缩包文件拖拽至此上传或点击上传</p>
            <p className="ant-upload-hint" style={{ margin: '0 54px' }}>
              支持的数据类型：pdf，图片格式（.jpg .jpeg .png
              .bmp）,支持以.zip格式上传数据，且文件大小不超过2G
            </p>
          </Dragger>
          <Table
            rowSelection={rowSelection}
            pagination={false}
            rowKey="uid"
            columns={columnsTable}
            dataSource={fileList}
          />
        </Modal>
        <Pagination
          size="small"
          current={pagination.current}
          total={pagination.total}
          pageSize={pagination.pageSize}
          showSizeChanger={false}
          onChange={this.getTableList}
        />
        {drawerTop}
      </div>
    );
  }
}

export default SceneList;
