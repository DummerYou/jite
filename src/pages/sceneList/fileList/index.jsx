import React, { PureComponent } from 'react';
import {
  Input, Button, Icon, DatePicker, Table ,Select
} from 'antd';
import { LeftOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import './style.scss';
@connect(({ sceneModel, loading }) => ({ ...sceneModel, loading }))
class ScenePageFileList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      sceneId: '',
      current: 1,
      size: 10,
      total: 0,
      dataSource:[],
    };
    console.log(this.props)
    this.addForm = React.createRef();
  }

  componentDidMount() {
    this.setState({
      sceneId:this.getQueryVariable('id')
    },()=>{
      this.search();
    })

  }
  // 返回
  toSceneManagement() {
      window.history.go(-1)
  }
  
  getQueryVariable(variable) {
    var query = this.props.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (pair[0] == variable) {
        return pair[1];
      }
    }
    return false;
  }
  search = (num) => {
    const {sceneId,current,size } = this.state;
    const { dispatch } = this.props;
    dispatch({
      type: 'sceneModel/selectFileInfo',
      payload: {
        "current":num || current,
        "size":size,
        "sceneId":sceneId //场景id
      },
    }).then(res => {
      if (res.code == 200) {
        var data = res.data;
        this.setState({
          current: data.current,
          size: data.size,
          total: data.total,
          dataSource: data.records,
        });
      } else {
      }
    });
  };
  openUrl(e){
    console.log(e)
    window.location.href = window.location.href.split('/fileList')[0] + "/operation?id="+e.sceneId + "&fileId="+e.id;
  }

  render() {
    const {dataSource,current, size, total } = this.state;
    const pagination = {
      current: current,
      pageSize: size,
      total: total,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current: date,
          },
          this.search(date),
        );
      },
    };
    const columns = [
      {
        title: 'id',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: '文件名称',
        dataIndex: 'fileName',
        key: 'fileName',
      },
      {
        title: '文件地址',
        dataIndex: 'filePath',
        key: 'filePath',
      },
      {
        title: '图片数量',
        dataIndex: 'imgNum',
        key: 'imgNum',
      },
      {
        title: '创建时间',
        dataIndex: 'createdTime',
        key: 'createdTime',
      },
      {
        title: '修改时间',
        dataIndex: 'updatedTime',
        key: 'updatedTime',
      },
      {
        title: '查看',
        dataIndex: 'check',
        key: 'check',
        render: (text, record) => <Button type="link" onClick={()=>this.openUrl(record)}>查看</Button>,
      },

    ];
    return (
      <div className="ScenePageFileList">
      <div className='to-scene'>
          <span className='hypc-link-span' onClick={this.toSceneManagement}>
              <LeftOutlined style={{ marginRight: 4 }} />
              返回场景中心
          </span>
      </div>
        <Table columns={columns} dataSource={dataSource} pagination={pagination} size="middle" />
      </div>
    );
  }
}

export default ScenePageFileList;
