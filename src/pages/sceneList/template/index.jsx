import React, { PureComponent } from 'react';
import { Input, Button, Icon, DatePicker, Table, Select,
  Modal,
  Form,
  Row,
  message,
  Col, } from 'antd';
import { LeftOutlined } from '@ant-design/icons';
import { connect } from 'dva';
import './style.scss';
const { confirm } = Modal;
const { TextArea } = Input;
@connect(({ sceneModel, loading }) => ({ ...sceneModel, loading }))
class ScenePageTemplate extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      sceneId: '',
      current: 1,
      size: 10,
      total: 0,
      dataSource: [],
      fileId: '',
      name: '',
      isModalVisible3:false,
      handlerScript: '',
      wastePageHandlerScript: '',
      backHandlerScript: '',
      matchingHandlerScript:'',
      thisId:null,
      templateName:"",
    };
    console.log(this.props);
    this.addForm = React.createRef();
  }

  componentDidMount() {
    this.search();
  }
  // 返回
  toSceneManagement() {
    try {
      window.history.go(-1)
    } catch (error) {
    }
  }

  getQueryVariable(variable) {
    var query = this.props.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (pair[0] == variable) {
        return pair[1];
      }
    }
    return false;
  }
  search = num => {
    console.log(num)
    const { sceneId, current, size, name, fileId } = this.state;
    const { dispatch } = this.props;
    dispatch({
      type: 'sceneModel/selectTemplateInfo',
      payload: {
        current: num || current,
        size: size,
        name: name,
        fileId: this.getQueryVariable('fileId'),
        sceneId: this.getQueryVariable('id'), //场景id
      },
    }).then(res => {
      if (res.code == 200) {
        var data = res.data;
        this.setState({
          current: data.current,
          size: data.size,
          total: data.total,
          dataSource: data.records,
        });
      } else {
      }
    });
  };
  showModal3 = (recordsItem={}) => {
    const reg = /\"(.*?)\"/;
    
    let wastePageHandlerScript = "";
    let matchingHandlerScript = "";
    if(recordsItem.wastePageHandlerScript && reg.exec(recordsItem.wastePageHandlerScript) && reg.exec(recordsItem.wastePageHandlerScript)[1]){
      wastePageHandlerScript = reg.exec(recordsItem.wastePageHandlerScript)[1]
    }
    if(recordsItem.matchingHandlerScript && reg.exec(recordsItem.matchingHandlerScript) && reg.exec(recordsItem.matchingHandlerScript)[1]){
      matchingHandlerScript = reg.exec(recordsItem.matchingHandlerScript)[1]
    }
    this.setState({
      thisId:recordsItem.id || null,
      isModalVisible3: true,
      wastePageHandlerScript,
      backHandlerScript: recordsItem.backHandlerScript || "",
      matchingHandlerScript,
      templateName:recordsItem.name || "",
    });
  };
  deleteClick = async(id,name) =>{
    const _this = this;
    confirm({
      title: '删除确认?',
      content: '是否删除'+name,
      onOk () {
        console.log(id);
        _this.deleteTemplateInfo(id)
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  deleteTemplateInfo = async(id) =>{
    const {dispatch} = this.props
    const res2 = await dispatch({
      type: 'sceneModel/deleteTemplateInfo',
      payload: {
        id
      },
    });
    if (res2 && res2.code == 200) {
      this.search();
    }
  }

  render() {
    
    
    const { dataSource, current, size, total,name,sceneId,fileId,isModalVisible3,handlerScript,
      wastePageHandlerScript,
      backHandlerScript,
      matchingHandlerScript,
      thisId,
      templateName, } = this.state;
      const pagination = {
      current: current,
      pageSize: size,
      total: total,
      showQuickJumper: true,
      onChange: (date, dateString) => {
        console.log(date);
        this.setState(
          {
            current: date,
          },
          this.search(date)
        );
      },
    };
    const columns = [
      {
        title: 'id',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: '模板名称',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '创建时间',
        dataIndex: 'createdTime',
        key: 'createdTime',
      },
      {
        title: '修改时间',
        dataIndex: 'updatedTime',
        key: 'updatedTime',
      },
      {
        title: '查看',
        dataIndex: 'check',
        key: 'check',
        render: (text, record) => <span>
        <Button type="link" onClick={()=>this.showModal3(record)}>修改</Button>
        <Button type="link" onClick={()=>this.deleteClick(record.id,record.name)}>删除</Button>
        </span>
      },
    ];
    const handleOk3 = async() => {
      const { dispatch} = this.props;
      try {
        let payloda = {
          id:thisId,
          fileId: this.getQueryVariable('fileId'),
          sceneId: this.getQueryVariable('id'), //场景id
          wastePageHandlerScript,
          backHandlerScript,
          matchingHandlerScript,
          name:templateName,
        }
        console.log(payloda)
        if(!payloda.name){
          message.error('请输入模板名称');
          return 
        }
        console.log(payloda)
        const res2 = await dispatch({
          type: 'sceneModel/saveTemplateInfo',
          payload: payloda,
        });
        if (res2 && res2.code == 200) {
          this.setState({
            isModalVisible3: false,
          });
          this.search();
        }
        
      } catch (e) {
        
      }
    };
    const handleCancel3 = () => {
      this.setState({ isModalVisible3: false });
    };
    return (
      <div className="ScenePageFileList">
        <div className="to-scene">
          <span className="hypc-link-span" onClick={this.toSceneManagement}>
            <LeftOutlined style={{ marginRight: 4 }} />
            返回
          </span>
        </div>
        <div className="ser">
          <Input placeholder="请输入模板名字" allowClear style={{ width: 200 }} value={name} onChange={(e)=>{this.setState({ name: e.target.value })}}/>
          <Button type="primary" onClick={()=>{this.search(1)}}>搜索</Button>
          <Button type="primary" style={{ marginLeft: '10px' }} onClick={()=>{this.showModal3()}}>添加</Button>
        </div>
        <Table columns={columns} dataSource={dataSource} pagination={pagination} size="middle" />
        <Modal visible={isModalVisible3} title="修改模板" onCancel={handleCancel3} onOk={handleOk3}>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={8}>模板名称:</Col>
            <Col span={16}>
              <Input
                type="textarea"
                value={templateName}
                onChange={e => this.setState({ templateName: e.target.value })}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={8}>匹配脚本字段:</Col>
            <Col span={16}>
              <TextArea
                rows={2}
                value={matchingHandlerScript}
                onChange={e => this.setState({ matchingHandlerScript: e.target.value })}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={8}>废页脚本字段:</Col>
            <Col span={16}>
              <TextArea
                rows={2}
                value={wastePageHandlerScript}
                onChange={e => this.setState({ wastePageHandlerScript: e.target.value })}
              />
            </Col>
          </Row>
          <Row style={{ marginBottom: '10px' }}>
            <Col span={8}>最终处理函数(data,text):</Col>
            <Col span={16}>
              <TextArea
                rows={2}
                value={backHandlerScript}
                onChange={e => this.setState({ backHandlerScript: e.target.value })}
              />
            </Col>
          </Row>
        </Modal>
      </div>
    );
  }
}
export default ScenePageTemplate;
