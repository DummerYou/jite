import React, { useEffect, useRef, useState } from 'react';
import { connect } from 'dva';
import { Button, Table, Popconfirm, message, Modal, Input, Form, Select,Tooltip } from 'antd';

const { Option } = Select;

function RoleList(props) {
  const { showAddUser, setShowAddUser } = props;

  /**
   * createdBy: "SYSTEM"
    createdTime: "2021-03-03 04:31:00"
    current: 0
    groupId: "6"
    id: 3
    permissions: null
    roleName: "无敌用户"
    size: 0
    sortField: null
    tclass: "com._4paradigm.invoice.record.manage.entity.Role"
    updatedBy: "SYSTEM"
    updatedTime: "2021-03-03 04:31:00"
   * 
   */
  const columns = [
    {
      title: '角色编号',
      dataIndex: 'id',
      key: 'id',
      width: 100,
    },
    {
      title: '角色名称',
      dataIndex: 'roleName',
      key: 'roleName',
      width: 120,
      render: field => <div className="text-ellipsis">{field}</div>,
    },
    {
      title: '所属机构',
      dataIndex: 'groupName',
      key: 'groupName',
      width: 120,
    },
    {
      title: '菜单功能权限',
      dataIndex: 'permissions',
      key: 'permissions',
      width: 200,
      render: field => (
        <span>
          {
            <Tooltip placement="top" title={field}>
              <span>{!field ? '' : field.length > 10 ? field.substring(0, 10) + '...' : field}</span>
            </Tooltip>
          }
        </span>
      ),
    },
    {
      title: '创建人',
      dataIndex: 'createdBy',
      key: 'createdBy',
      width: 120,
    },
    {
      title: '创建时间',
      dataIndex: 'createdTime',
      key: 'createdTime',
      width: 180,
    },
    {
      title: '最近修改时间',
      dataIndex: 'updatedTime',
      key: 'updatedTime',
      width: 180,
    },
    {
      title: '最近修改人',
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      width: 120,
    },
  ];

  const [list, setList] = useState([]);
  const [pagination, setPagination] = useState({ current: 1, total: 0, pageSize: 5 });
  const [curUser, setEditUser] = useState();
  const [showPwdModal, setShowPwdModal] = useState(false);
  const formRef = useRef();
  const pwdForm = useRef();
  useEffect(() => {
    getList();
  }, [pagination.current]);

  // 属性监听
  useEffect(() => {
    !showPwdModal && setEditUser(null);
  }, [showPwdModal]);

  useEffect(() => {
    !showAddUser && setEditUser(null);
  }, [showAddUser]);

  const getList = async () => {
    try {
      const { records, size, total, current } = await props.dispatch({
        type: 'systemModel/getRoleList',
        payload: { size: pagination.pageSize, current: pagination.current, groupId: 1 },
      });
      setList(records);
      setPagination({ pageSize: size, total, current });
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const handleSubmit = async () => {
    try {
      const values = await formRef.current.validateFields();
      console.log(values);
      if (values.password !== values.confirmPwd) {
        return message.error('前后密码不一致');
      }
      delete values.confirmPwd;
      curUser ? await editUser({ ...values, id: curUser.id }) : await addUser(values);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };
  // {"username":"8889","password":"123","nickName":"好名字","groupId":"1","roleIds":[2]}
  const addUser = async userInfo => {
    try {
      await props.dispatch({
        type: 'systemModel/addUser',
        payload: userInfo,
      });
      message.success('添加成功');
      getList();
      setShowAddUser(false);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const editUser = async userInfo => {
    try {
      await props.dispatch({
        type: 'systemModel/editUser',
        payload: userInfo,
      });
      message.success('保存成功');
      getList();
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const resetPassword = async () => {
    try {
      const values = await pwdForm.current.validateFields();
      await props.dispatch({
        type: 'systemModel/resetPassword',
        payload: { id: curUser.id, ...values },
      });
      message.success('密码重置成功');
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const onPageChange = page => {
    setPagination({ ...pagination, current: page.current });
  };

  return (
    <div>
      <Table
        dataSource={list}
        columns={columns}
        pagination={pagination}
        onChange={onPageChange}
        scroll={{ y: 500 }}
        rowKey="id"
      />
      <Modal
        title="重置密码"
        onOk={resetPassword}
        visible={showPwdModal}
        onCancel={() => setShowPwdModal(false)}
      >
        <Form labelCol={{ span: 4 }} ref={pwdForm}>
          <Form.Item
            name="newPassword"
            label="密码"
            rules={[{ required: true, message: '请输入' }]}
          >
            <Input.Password placeholder="请输入" />
          </Form.Item>
        </Form>
      </Modal>
      <Modal
        visible={showAddUser}
        onCancel={() => setShowAddUser(false)}
        title={`${curUser ? '编辑' : '添加'}用户`}
        onOk={handleSubmit}
        width={600}
        destroyOnClose
      >
        <Form
          labelCol={{ span: 4 }}
          ref={formRef}
          initialValues={
            curUser && {
              username: curUser.username,
              nickName: curUser.nickName,
              groupId: curUser.groupId,
              roleIds: curUser.roleIds,
            }
          }
        >
          <Form.Item name="username" label="用户名" rules={[{ required: true, message: '请输入' }]}>
            <Input placeholder="请输入" />
          </Form.Item>
          <Form.Item
            name="nickName"
            label="昵称"
            rules={[{ required: true, message: '请输入新密码' }]}
          >
            <Input placeholder="请输入" />
          </Form.Item>
          {!curUser && (
            <>
              <Form.Item
                name="password"
                label="密码"
                rules={[{ required: true, message: '请输入' }]}
              >
                <Input.Password placeholder="请输入" />
              </Form.Item>
              <Form.Item
                name="confirmPwd"
                label="确认密码"
                rules={[{ required: true, message: '请输入' }]}
              >
                <Input.Password placeholder="请输入" />
              </Form.Item>
            </>
          )}

          <Form.Item
            name="groupId"
            label="所属部门"
            rules={[{ required: true, message: '请选择' }]}
          >
            <Select allowClear={true}>
              <Option value="1">财务</Option>
              <Option value="2">行政</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="roleIds"
            label="配置角色"
            rules={[{ required: true, message: '请输入' }]}
          >
            <Select mode="multiple" allowClear={true}>
              <Option value="1">主管</Option>
              <Option value="2">科员</Option>
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default connect(({ systemModel }) => ({ ...systemModel }))(RoleList);
