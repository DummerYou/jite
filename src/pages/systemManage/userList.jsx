import React, { useEffect, useRef, useState } from 'react';
import { connect } from 'dva';
import { Button, Table, message, Modal, Input, Form, Select, TreeSelect,Tooltip } from 'antd';

const { Option } = Select;

function UserList(props) {
  const { showAddUser, setShowAddUser, curGroup, groupTreeData } = props;
  const columns = [
    {
      title: '编号',
      dataIndex: 'index',
      key: 'index',
      width: 100,
      render: (_, record, index) => <span>{index + 1}</span>,
    },
    {
      title: '登录账号',
      dataIndex: 'username',
      key: 'username',
      width: 120,
    },
    {
      title: '姓名',
      dataIndex: 'nickName',
      key: 'nickName',
      width: 120,
    },
    {
      title: '所属机构',
      dataIndex: 'groupName',
      key: 'groupName',
      width: 120,
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: 80,
      render: field => <span>{['正常', '禁用'][field]}</span>,
    },
    {
      title: '拥有角色',
      dataIndex: 'roles',
      key: 'roles',
      width: 200,
      render: field => (
        <span>
          {
            <Tooltip placement="top" title={field}>
              <span>{!field ? '' : field.length > 10 ? field.substring(0, 10) + '...' : field}</span>
            </Tooltip>
          }
        </span>
      ),
    },
    {
      title: '创建人',
      dataIndex: 'createdBy',
      key: 'createdBy',
      width: 120,
    },
    {
      title: '创建时间',
      dataIndex: 'createdTime',
      key: 'createdTime',
      width: 200,
    },
    {
      title: '最近修改时间',
      dataIndex: 'updatedTime',
      key: 'updatedTime',
      width: 200,
    },
    {
      title: '操作',
      align:'center',
      dataIndex: 'action',
      key: 'action',
      fixed: 'right',
      width: 180,
      render: (field, record) => (
        <span>
          <div className="flex-hc">
            <Button
              type="link"
              onClick={() => {
                setEditUser(record);
                setShowAddUser(true);
              }}
            >
              编辑
            </Button>
            <Button
              type="link"
              onClick={() => {
                setEditUser(record);
                setShowPwdModal(true);
              }}
            >
              重置密码
            </Button>
          </div>
          <div className="flex-hc">
            <Button type="link" onClick={() => delUser(record)}>
              删除
            </Button>
            <Button type="link" onClick={() => userForbidden(record)}>
              {record.status == 1 ? '启用' : '禁用'}
            </Button>
          </div>
        </span>
      ),
    },
  ];

  const [list, setList] = useState([]);
  const [pagination, setPagination] = useState({ current: 1, total: 0, pageSize: 5 });
  const [curUser, setEditUser] = useState();
  const [showPwdModal, setShowPwdModal] = useState(false);
  const [roleList, setRoleList] = useState([]);
  const formRef = useRef();
  const pwdForm = useRef();
  useEffect(() => {
    getList();
  }, [pagination.current, curGroup]);

  // 属性监听
  useEffect(() => {
    !showPwdModal && setEditUser(null);
  }, [showPwdModal]);

  useEffect(() => {
    !showAddUser && setEditUser(null);
  }, [showAddUser]);

  // 获取角色列表
  useEffect(() => {
    const getRoleList = async () => {
      try {
        const { records } = await props.dispatch({
          type: 'systemModel/getRoleList',
          payload: {
            size: 100,
          },
        });
        setRoleList(records);
      } catch (error) {
        error.message && message.error(error.message);
      }
    };

    getRoleList();
  }, []);

  const getList = async () => {
    try {
      const { records, size, total, current } = await props.dispatch({
        type: 'systemModel/getUserList',
        payload: {
          size: pagination.pageSize,
          current: pagination.current,
          groupId: curGroup.groupId,
        },
      });
      setList(records);
      setPagination({ pageSize: size, total, current });
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const delUser = async user => {
    const modal = Modal.confirm({
      title: '提示',
      icon: null,
      content: '是否确认删除?',
      okText: '确认',
      cancelText: '取消',
      onOk: async () => {
        try {
          await props.dispatch({
            type: 'systemModel/deleteUser',
            payload: { id: user.id },
          });
          message.success('删除成功');
          getList();
          modal.destroy();
        } catch (error) {
          error.message && message.error(error.message);
        }
      },
    });
  };

  const handleSubmit = async () => {
    try {
      const values = await formRef.current.validateFields();
      console.log(values);
      if (values.password !== values.confirmPwd) {
        return message.error('前后密码不一致');
      }
      delete values.confirmPwd;
      curUser ? await editUser({ ...values, id: curUser.id }) : await addUser(values);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };
  // {"username":"8889","password":"123","nickName":"好名字","groupId":"1","roleIds":[2]}
  const addUser = async userInfo => {
    try {
      await props.dispatch({
        type: 'systemModel/addUser',
        payload: userInfo,
      });
      message.success('添加成功');
      getList();
      setShowAddUser(false);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const editUser = async userInfo => {
    try {
      await props.dispatch({
        type: 'systemModel/editUser',
        payload: userInfo,
      });
      message.success('保存成功');
      getList();
      setShowAddUser(false);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const userForbidden = async user => {
    try {
      await props.dispatch({
        type: 'systemModel/userForbidden',
        payload: { id: user.id },
      });
      message.success('操作成功');
      getList();
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const resetPassword = async () => {
    try {
      const values = await pwdForm.current.validateFields();
      await props.dispatch({
        type: 'systemModel/resetPassword',
        payload: { id: curUser.id, ...values },
      });
      message.success('密码重置成功');
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const onPageChange = page => {
    setPagination({ ...pagination, current: page.current });
  };

  return (
    <div>
      <Table
        dataSource={list}
        columns={columns}
        pagination={pagination}
        onChange={onPageChange}
        scroll={{ y: 500 }}
      />
      <Modal
        title="重置密码"
        onOk={resetPassword}
        visible={showPwdModal}
        onCancel={() => setShowPwdModal(false)}
      >
        <Form labelCol={{ span: 4 }} ref={pwdForm}>
          <Form.Item
            name="newPassword"
            label="密码"
            rules={[{ required: true, message: '请输入' }]}
          >
            <Input.Password placeholder="请输入" />
          </Form.Item>
        </Form>
      </Modal>
      <Modal
        visible={showAddUser}
        onCancel={() => setShowAddUser(false)}
        title={`${curUser ? '编辑' : '添加'}用户`}
        onOk={handleSubmit}
        width={600}
        destroyOnClose
      >
        <Form
          labelCol={{ span: 4 }}
          ref={formRef}
          initialValues={
            curUser && {
              username: curUser.username,
              nickName: curUser.nickName,
              groupId: curUser.groupId,
              roleIds: curUser.roleIds||[],
            }
          }
        >
          <Form.Item name="username" label="用户名" rules={[{ required: true, message: '请输入' }]}>
            <Input placeholder="请输入" />
          </Form.Item>
          <Form.Item
            name="nickName"
            label="昵称"
            rules={[{ required: true, message: '请输入新密码' }]}
          >
            <Input placeholder="请输入" />
          </Form.Item>
          {!curUser && (
            <>
              <Form.Item
                name="password"
                label="密码"
                rules={[{ required: true, message: '请输入' }]}
              >
                <Input.Password placeholder="请输入" />
              </Form.Item>
              <Form.Item
                name="confirmPwd"
                label="确认密码"
                rules={[{ required: true, message: '请输入' }]}
              >
                <Input.Password placeholder="请输入" />
              </Form.Item>
            </>
          )}

          <Form.Item
            name="groupId"
            label="所属部门"
            rules={[{ required: true, message: '请选择' }]}
          >
            <TreeSelect
              dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
              placeholder="请选择部门"
              allowClear
              treeDefaultExpandAll
              treeData={groupTreeData}
            />
          </Form.Item>
          <Form.Item
            name="roleIds"
            label="配置角色"
            rules={[{ required: true, message: '请输入' }]}
          >
            <Select mode="multiple" allowClear={true}>
              {roleList.map((role, i) => (
                <Option value={role.id}>{role.roleName+'('+(!role.groupName?'':role.groupName)+')'}</Option>
              ))}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default connect(({ systemModel }) => ({ ...systemModel }))(UserList);
