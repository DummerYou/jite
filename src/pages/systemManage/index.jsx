import React, { useMemo, useEffect, useState, useRef } from 'react';
import {
  Row,
  Col,
  Menu,
  Tree,
  Select,
  Modal,
  Button,
  Tabs,
  Form,
  Input,
  
  message,
  Card,
  List,
} from 'antd';
import { connect } from 'dva';
import './style.scss';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import UserList from './userList';
import RoleList from './roleList';

const { DirectoryTree, TreeNode } = Tree;
const { Option } = Select;

const { TabPane } = Tabs;

function SystemManage(props) {
  const [treeData, setTreeData] = useState([]);
  const [tabkey, setTabkey] = useState('1');
  const [showAddUser, setShowAddUser] = useState(false);
  const [editNode, setEditNode] = useState();
  const [curGroup, setCurGroup] = useState({});
  const formRef = useRef();

  const processTreeData = (datas = [], prefixKey = '') => {
    datas.forEach((item, i) => {
      // item.key = `${prefixKey ? prefixKey + '-' : ''}${i}`;
      item.key = item.groupId;
      item.title = renderTreeTitle(item);
      if (item.children) {
        processTreeData(item.children, item.key);
      }
    });
  };

  useEffect(() => {
    getGroupTree();
  }, []);

  const getGroupTree = async () => {
    try {
      const data = await props.dispatch({ type: 'systemModel/getAllGroup', payload: {} });
      processTreeData(data);
      setTreeData(data);
      setCurGroup(data[0] || {});
      console.log(data);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const onTreeDelete = (e, item) => {
    e.stopPropagation();
    Modal.confirm({
      title: '提示',
      icon: null,
      content: '是否确认删除?',
      okText: '确认',
      cancelText: '取消',
      onOk: async () => {
        try {
          await props.dispatch({ type: 'systemModel/deleteGroup', payload: { id: item.id } });
          message.success('删除成功');
          getGroupTree();
        } catch (error) {
          error.message && message.error(error.message);
        }
      },
    });
  };

  const onTreeEdit = (e, item) => {
    e.stopPropagation();
    setEditNode(item);
  };

  const renderTreeTitle = item => {
    return (
      <div className="tree-title">
        <span>{item.groupName}</span>
        <span className="tree-icons">
          {/* <DeleteOutlined onClick={e => onTreeDelete(e, item)} />
          <EditOutlined className="ml-10" onClick={e => onTreeEdit(e, item)} /> */}
        </span>
      </div>
    );
  };

  const onTabChange = key => {
    setTabkey(key);
  };

  const onEditGroupNode = async () => {
    try {
      const values = await formRef.current.validateFields();
      values.id = editNode.id;
      await props.dispatch({ type: 'systemModel/editGroup', payload: values });
      message.success('修改成功');
      getGroupTree();
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const onTreeNodeSelect = (selectedKeys, { selected, node }) => {
    setCurGroup(node);
  };

  const groupInfos = [
    { title: '机构名称', key: 'groupName' },
    { title: '机构编号', key: 'groupId' },
    { title: '创建人', key: 'createdBy' },
    { title: '最新修改人', key: 'updatedBy' },
    { title: '最近修改时间', key: 'updatedTime' },
    { title: '创建时间', key: 'createdTime' },
    { title: '描述', key: 'description' },
  ];

  const getGroupDetail = async () => {
    try {
      await props.dispatch({ type: 'systemModel/getGroupDetail', payload: { id: 1 } });
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  return (
    <div className="manage-page">
      <div className="left-menu">
        <Menu
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode="inline"
          theme="dark"
          inlineCollapsed={false}
          className="menu-list"
        >
          <Menu.Item key="1">机构管理</Menu.Item>
        </Menu>
      </div>
      <h2>机构管理</h2>
      <Row>
        <Col span={5}>
          <DirectoryTree
            defaultSelectedKeys="0"
            defaultExpandAll
            treeData={treeData}
            style={{ marginTop: '15px' }}
            onSelect={onTreeNodeSelect}
          />
        </Col>
        <Col span={18} offset={1}>
          <Tabs
            type="card"
            onChange={onTabChange}
            activeKey={tabkey}
            tabBarExtraContent={
              tabkey === '3' ? (
                <Button type="primary" onClick={() => setShowAddUser(true)}>
                  创建用户
                </Button>
              ) : null
            }
          >
            <TabPane tab="机构信息" key="1">
              <div>
                <List
                  grid={{ gutter: 16, column: 3 }}
                  dataSource={groupInfos}
                  renderItem={item => (
                    <List.Item>
                      <span>{item.title}</span>：<span>{curGroup[item.key]}</span>
                    </List.Item>
                  )}
                />
              </div>
            </TabPane>
            <TabPane tab="角色管理" key="2">
              <RoleList />
            </TabPane>
            <TabPane tab="用户管理" key="3">
              <UserList
                showAddUser={showAddUser}
                setShowAddUser={setShowAddUser}
                curGroup={curGroup}
                groupTreeData={treeData}
              />
            </TabPane>
          </Tabs>
        </Col>
      </Row>

      <Modal
        visible={editNode}
        title="编辑"
        onCancel={() => setEditNode(null)}
        onOk={onEditGroupNode}
        destroyOnClose
      >
        <Form
          labelCol={{ span: 2 }}
          wrapperCol={{ offset: 1 }}
          ref={formRef}
          initialValues={{ name: editNode && editNode.groupName }}
        >
          <Form.Item name="name" label="部门" rules={[{ required: true }]}>
            <Input placeholder="请输入" />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}

export default connect(({ systemModel }) => ({ ...systemModel }))(SystemManage);
