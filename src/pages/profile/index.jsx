import React, { PureComponent } from 'react';
import {
  Row,
  Col,
  Tree,
  Icon,
  Table,
  Button,
  Select,
  Input,
  Modal,
  Upload,
  message,
  List,
  Form,
  TreeSelect,
  Dropdown,
  Menu,
  Tooltip,
} from 'antd';
import { connect } from 'dva';
import { PlusOutlined, DeleteOutlined, EditOutlined, LoadingOutlined } from '@ant-design/icons';
import PreviewImage from '../../components/PreviewImage';
import './style.scss';
import { uploadTicket } from '../../api/index';
import cameraImg from '../../assets/camera.png';

const { DirectoryTree } = Tree;
const { Option } = Select;
const IMG_PREFIX = '/data-manage/image/';
const DOWN_PREFIX = '/data-manage/down/';
@connect(({ profileModel, loading }) => ({ ...profileModel, loading }))
class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedTreeKeys: [],
      expandedTreeKeys: [],
      foldTreeData: [],
      groupTreeData: [],
      treeNode: null,
      showAddModal: false,
      addSubNode: false, // 添加子节点
      tableList: [], // table数据
      invoiceTypes: [], // 票据类型
      pagination: { current: 1, total: 0, pageSize: 5 }, // 分页
      dataManageVo: [
        { title: '票据类型', key: 'typeName', value: 1, edit: true },
        // { title: '名称', key: 'name', edit: true },
        { title: '归属部门', key: 'groupName', edit: true },
        { title: '存放位置', key: 'position', edit: true },
        { title: '备注', key: 'description', edit: true },
        { title: '上传人', key: 'createdBy' },
        { title: '上传时间', key: 'createdTime' },
        { title: '最新确认人', key: 'updatedBy' },
        { title: '最新确认时间', key: 'updatedTime' },
      ],
      showTicketDetail: false,
      folderId: '', // 当前选中的节点id
      groupId: '', // 选中的部门id
      uploadType: '',
      uploadTicketList: [],
      currentIndex: 0, // 当前校验票据
      uploading: false,
      showUploader: true,
      sceneID:0,
    };
    this.addForm = React.createRef();
  }

  columns = [
    {
      title: '文件名称',
      dataIndex: 'fileName',
      key: 'fileName',
      width: 120,
      className: 'cloumns-money',
      render: field => (
        <span>
          {
            <Tooltip placement="top" title={field}>
              <span>{!field ? '' : field.length > 8 ? field.substring(0, 8) + '...' : field}</span>
            </Tooltip>
          }
        </span>
      ),
    },
    {
      title: '档案类型',
      dataIndex: 'type',
      key: 'type',
      render: field => <span>{['上传文件', '电子化识别'][field]}</span>,
      width: 100,
      className: 'cloumns-money',
    },
    {
      title: '票据类型',
      dataIndex: 'invoiceManageVo',
      key: 'invoiceManageVo',
      render: red => <span>{!red ? '' : red.typeName}</span>,
      width: 100,
      className: 'cloumns-money',
    },
    {
      title: '存放位置',
      dataIndex: 'position',
      key: 'position',
      width: 100,
      className: 'cloumns-money',
    },
    {
      title: '归属部门',
      dataIndex: 'groupName',
      key: 'groupName',
      width: 130,
      className: 'cloumns-money',
    },
    {
      title: '文件大小',
      dataIndex: 'fileSize',
      key: 'fileSize',
      width: 100,
      className: 'cloumns-money',
    },
    {
      title: '上传人',
      dataIndex: 'createdBy',
      key: 'createdBy',
      width: 100,
      className: 'cloumns-money',
    },
    {
      title: '上传时间',
      dataIndex: 'createdTime',
      key: 'createdTime',
      width: 170,
      className: 'cloumns-money',
    },
    {
      title: '最近修改人',
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      width: 100,
      className: 'cloumns-money',
    },
    {
      title: '最近修改时间',
      dataIndex: 'updatedTime',
      key: 'updatedTime',
      width: 170,
      className: 'cloumns-money',
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'action',
      key: 'action',
      width: 190,
      fixed: 'right',
      className: 'cloumns-money',
      render: (field, record) => (
        <span style={{ display: 'flex' }}>
          {record.type == 0 ? (
            ''
          ) : (
            <Button type="link" onClick={() => this.getInvoiceDetail(record)}>
              详情
            </Button>
          )}
          <Button type="link" onClick={() => this.handleDelTicket(record)}>
            删除
          </Button>
          <Button type="link" onClick={() => this.onDownload(record)}>
            下载
          </Button>
        </span>
      ),
    },
  ];

  componentDidMount() {
    this.getTreeData();
    // this.getTableList();
    this.getInvoiceType();
    this.getGroupTree();
  }

  async getTreeData() {
    const { dispatch } = this.props;
    try {
      const data = await dispatch({ type: 'profileModel/getTreeData', payload: {} });
      if (!data) return;
      this.processTreeData(data);
      this.setState({ foldTreeData: data, folderId: data.length > 0 ? data[0].id : '' });
      this.getTableList();
    } catch (error) {
      error.message && message.error(error.message);
    }
  }

  // 部门数据  "Cannot read property '0' of null"
  getGroupTree = async () => {
    try {
      const data = await this.props.dispatch({ type: 'systemModel/getAllGroup', payload: {} });
      if (!data) return;
      this.processGroupData(data);
      this.setState({ groupTreeData: data });
      //   console.log('groupdata', data);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  processGroupData = (datas = [], prefixKey = '') => {
    (datas || []).forEach((item, i) => {
      item.key = `${prefixKey ? prefixKey + '-' : ''}${i}`;
      item.title = item.groupName;
      item.value = item.groupId;
      if (item.children) {
        this.processGroupData(item.children, item.key);
      }
    });
  };

  getTableList = async () => {
    const { dispatch } = this.props;
    try {
      const { pagination, folderId } = this.state;
      const res = await dispatch({
        type: 'profileModel/getTableList',
        payload: {
          size: pagination.pageSize,
          current: pagination.current,
          folderId,
        },
      });
      if (!res) return;
      const { records, size, total, current } = res;
      this.setState({ tableList: records || [], pagination: { pageSize: size, total, current } });
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  onDownload = record => {
    const image = DOWN_PREFIX + '?id=' + record.id;

    // console.log(image);
    const a = document.createElement('a');
    // a.download = 'img.png';
    a.href = image;
    a.target = '_blank';
    a.click();
  };

  // 删除票据
  handleDelTicket = item => {
    const modal = Modal.confirm({
      title: '提示',
      icon: null,
      content: '是否确认删除?',
      okText: '确认',
      cancelText: '取消',
      onOk: async () => {
        try {
          const { dispatch } = this.props;
          await dispatch({ type: 'profileModel/deleteTicket', payload: { id: item.id } });
          this.getTableList();
          message.success('删除成功');
          modal.destroy();
        } catch (error) {
          error.message && message.error(error.message);
        }
      },
    });
  };

  async getInvoiceType() {
    const { dispatch } = this.props;
    try {
      const data = await dispatch({
        type: 'profileModel/selectInvoiceType',
        payload: { size: 100 },
      });
      if (!data) return;
      this.setState({ invoiceTypes: data.records });
    } catch (error) {
      error.message && message.error(error.message);
    }
  }

  processTreeData(treeData, prefixKey = '') {
    (treeData || []).forEach((item, i) => {
      item.key = `${prefixKey ? prefixKey + '-' : ''}${i}`;
      item.title = this.renderTreeTitle(item);
      if (item.children) {
        this.processTreeData(item.children, item.key);
      }
    });
  }

  onTreeDelete = (e, item) => {
    e.stopPropagation();
    const modal = Modal.confirm({
      title: '提示',
      icon: null,
      content: '是否确认删除?',
      okText: '确认',
      cancelText: '取消',
      onOk: async () => {
        try {
          await this.props.dispatch({
            type: 'profileModel/delFolder',
            payload: { id: item.id },
          });
          this.getTreeData();
          message.success('删除成功');
          modal.destroy();
        } catch (error) {
          error.message && message.error(error.message);
        }
      },
    });
  };
  uploadScess =(info) =>{
    // e.stopPropagation();
    const length = info.fileList.length
    const modal = Modal.confirm({
      icon: null,
      content: `你正在上传`+length + `份合同，稍后可到信息检索中心了解提取结果,确定继续吗？点击"取消"返回主界面重新上传。`,
      okText: '确定',
      cancelText: '取消',
      onOk: () => {
          console.log('ok');
      },
      onCancel() {
          console.log('Cancel');
      },
    });
  }

  // 编辑当前节点
  onTreeEdit = (e, item) => {
    e.stopPropagation();
    this.setState({ showAddModal: true, treeNode: item, addSubNode: false });
  };

  // 添加子节点
  onTreeAdd = (e, item) => {
    e.stopPropagation();
    this.setState({ showAddModal: true, treeNode: item, addSubNode: true });
  };

  renderTreeTitle = item => {
    return (
      <div className="tree-title">
        <span>{!item.prefix ? item.name : item.name + '（' + item.prefix + '）'}</span>
        <span className="tree-icons">
          <DeleteOutlined onClick={e => this.onTreeDelete(e, item)} />
          <EditOutlined className="ml-10 mr-10" onClick={e => this.onTreeEdit(e, item)} />
          <PlusOutlined onClick={e => this.onTreeAdd(e, item)} />
        </span>
      </div>
    );
  };

  getTreeNodeByID = (treeList, id) => {
    for (let i = 0; i < treeList.length; i++) {
      let ele = treeList[i];
      if (ele.id == id) {
        return ele;
      } else if (ele.children && ele.children.length > 0) {
        let node = this.getTreeNodeByID(ele.children, id);
        if (node) return node;
      }
    }
  };

  getTreeIcon = (data, item) => {
    const { selected, expanded } = data;
    return (
      <Icon
        type={expanded && item.children.length > 0 ? 'folder-open' : 'folder'}
        theme="filled"
        style={{ fontSize: '16px', color: selected ? '#ffffff' : '#1890ff' }}
      ></Icon>
    );
  };

  getModalTitle = () => {
    const { isEditModal } = this.state;
    const type = isEditModal ? 'edit' : 'plus-circle';
    const title = isEditModal ? '编辑名称' : '添加子部门';
    return (
      <div>
        <Icon type={type} style={{ color: '#1890ff', marginRight: '10px' }} />
        {title}
      </div>
    );
  };

  // 添加/ 编辑
  handleAddFolder = async () => {
    const { dispatch } = this.props;
    const { treeNode, addSubNode } = this.state;
    try {
      const values = await this.addForm.current.validateFields();
      if (treeNode) {
        addSubNode ? (values.parentId = treeNode.id) : (values.id = treeNode.id);
      }
      await dispatch({ type: 'profileModel/addFolder', payload: values });
      this.getTreeData();
      this.setState({ showAddModal: false });
      message.success(!addSubNode ? '修改成功' : '添加成功');
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  onPageChange = page => {
    this.setState(
      { pagination: { ...this.state.pagination, current: page.current } },
      this.getTableList
    );
  };

  // 文件上传信息
  uploadProps = {
    name: 'files',
    action: '/data-manage/uploadInvoice',
    headers: {
      token: localStorage.getItem('TOKEN'),
    },
    data: { type: 1 },
    showUploadList: false,
    onChange: info => {
      this.setState({ uploading: true });
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        if ([0, 200].includes(info.file.response.code)) {
          if (typeof info.file.response.data[0] == 'string') {
            this.getTableList();
            message.success('上传成功');
            this.setState({ uploading: false });
            return;
          }
          // 上传成功
          if (info.fileList.length === 1) {
            if(this.state.sceneID===0){
              this.setState({
                uploadTicketList: [this.getTicketInfo(info.file.response.data)],
                showTicketDetail: true,
                uploading: false,
                currentIndex: 0,
              });
              }else{
                this.uploadScess(info)
              }
            return;
          }

          // 多文件
          const { uploadTicketList } = this.state;
          console.log('111',this.state)
          if(this.state.sceneID===0){
            const scanTicket = this.getTicketInfo(info.file.response.data);
            uploadTicketList.push(scanTicket);
            this.setState({ uploadTicketList });
              if (uploadTicketList.length === info.fileList.length) {
                console.log('多文件上传成功', info.fileList.length);
                  this.setState({ showTicketDetail: true, uploading: false, currentIndex: 0 });
              }
            }else{
                    uploadTicketList.push({});
                    if (uploadTicketList.length === info.fileList.length) {
                      // console.log('多文件上传成功', info.fileList.length);
                        // this.setState({ showTicketDetail: true, uploading: false, currentIndex: 0 });
                        this.uploadScess(info)
                    }
            }
        } else {
          message.error(info.file.response.message);
          this.setState({ uploading: false });
        }
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
        this.setState({ uploading: false });
      }
    },
  };

  getTicketInfo = datas => {
    const uploadTicketInfo = Object.entries(datas[0].elementMap).map(([key, val], i) => ({
      title: key,
      value: val,
    }));

    const scanTicket = {
      ticketEditable: true,
      scanData: uploadTicketInfo, // 扫描数据
      basicInfo: [
        { title: '票据类型', key: 'typeName', value: this.state.uploadTypeName, edit: true },
        // { title: '名称', key: 'name', edit: true },
        { title: '归属部门', key: 'groupId', value: datas[0].groupId, edit: true },
        { title: '存放位置', key: 'position', edit: true },
        { title: '备注', key: 'description', edit: true },
        { title: '上传人', key: 'createdBy' },
        { title: '上传时间', key: 'createdTime' },
        { title: '最新确认人', key: 'updatedBy' },
        { title: '最新确认时间', key: 'updatedTime' },
      ], // 基础信息
      ...datas[0],
    };
    return scanTicket;
  };

  // 校验完成
  onCheckFinish = async () => {
    const { dispatch } = this.props;
    const { folderId, uploadTicketList, currentIndex, uploadType } = this.state;
    const {
      scanData: uploadTicketInfo,
      basicInfo: dataManageVoInfo,
      ticketEditable,
      fileName,
      name,
      url,
      groupId,
    } = uploadTicketList[currentIndex];
    if (!ticketEditable) return false;

    // 编辑状态
    try {
      const schemaData = JSON.stringify(
        uploadTicketInfo.reduce((o, cur) => {
          o[cur.title] = cur.value;
          return o;
        }, {})
      );
      const dataManageVo = dataManageVoInfo
        .filter(v => v.edit)
        .reduce((o, cur) => {
          o[cur.key] = cur.value;
          return o;
        }, {});
      dataManageVo.type = 1; // 电子化识别
      dataManageVo.url = url; // 上传文件地址
      dataManageVo.folderId = folderId;
      dataManageVo.name = name;
      dataManageVo.fileName = fileName;

      const payload = {
        type: uploadType, // 上传识别
        schemaData,
        dataManageVo,
        url,
        groupId: dataManageVo.groupId,
      };
      await dispatch({ type: 'profileModel/saveInvoiceInfo', payload });
      message.success('校验成功');
      return true;
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  handleModalConfirm = async () => {
    const res = await this.onCheckFinish();
    if (res) {
      this.setState({ showUploader: false }, () => {
        this.setState({ showTicketDetail: false, showUploader: true, currentIndex: 0 });
        this.getTableList();
      });
    }else{
      this.onModalClose();
    }
  };

  onTicketInfoInputChange = (value, index) => {
    const { uploadTicketList, currentIndex } = this.state;
    const newList = [...uploadTicketList];
    newList[currentIndex].scanData[index].value = value;
    this.setState({ uploadTicketList: newList });
  };

  onManageInputChange = (value, index) => {
    const { uploadTicketList, currentIndex } = this.state;
    const newList = [...uploadTicketList];
    newList[currentIndex].basicInfo[index].value = value;
    this.setState({ uploadTicketList: newList });
  };

  onTreeNodeSelect = (selectedKeys, { selected, node }) => {
    const { folderId, pagination } = this.state;
    node.id !== folderId &&
      this.setState(
        { folderId: node.id, pagination: { ...pagination, current: 1 } },
        this.getTableList
      );
  };

  // 部门
  renderTreeSelect = (index, disabled) => {
    const { uploadTicketList, currentIndex } = this.state;
    const { basicInfo } = uploadTicketList[currentIndex];
    return (
      <TreeSelect
        style={{ width: '100%' }}
        value={basicInfo[1].value}
        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
        placeholder="请选择部门"
        allowClear
        treeDefaultExpandAll
        disabled={disabled}
        onChange={value => {
          const newList = [...uploadTicketList];
          newList[currentIndex].basicInfo[index].value = value;
          this.setState({ uploadTicketList: newList });
        }}
        suffixIcon={<EditOutlined />}
        treeData={this.state.groupTreeData}
      />
    );
  };

  // 获取票据详情
  getInvoiceDetail = async item => {
    const { dispatch } = this.props;
    const dataManageVo = [...this.state.dataManageVo];

    try {
      const res = await dispatch({
        type: 'profileModel/getInvoiceDetail',
        payload: { id: item.invoiceManageVo.id },
      });
      const schemaData = JSON.parse(res.schemaData);
      const uploadTicketInfo = Object.entries(schemaData).map(([key, val], i) => ({
        title: key,
        value: val,
      }));
      dataManageVo.forEach(v => (v.value = res[v.key]));
      const ticketData = {
        ticketEditable: false,
        scanData: uploadTicketInfo,
        basicInfo: dataManageVo,
        url: res.url,
      };

      this.setState({
        showTicketDetail: true,
        uploadTicketList: [ticketData],
      });
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  // 下一张票据
  onNextTicket = async () => {
    let { uploadTicketList, currentIndex } = this.state;
    const { ticketEditable } = uploadTicketList[currentIndex];
    if (!ticketEditable) {
      // 不可编辑，切换到下一张
      this.setState({ currentIndex: ++currentIndex });
      return;
    }

    // 保存校验
    const res = await this.onCheckFinish();
    if (res) {
      if (currentIndex < uploadTicketList.length - 1) {
        const newList = [...uploadTicketList];
        newList[currentIndex].ticketEditable = false;
        this.setState({ currentIndex: ++currentIndex, uploadTicketList: newList });
      } else {
        // 最后一张
        this.setState({ showUploader: false }, () => {
          this.setState({
            showTicketDetail: false,
            uploadTicketList: [],
            showUploader: true,
            currentIndex: 0,
          });
          this.getTableList();
        });
      }
    }
  };

  onPreTicket = () => {
    let { currentIndex } = this.state;
    this.setState({ currentIndex: --currentIndex });
  };

  // 拍照上传
  onTakePhotoChange = async e => {
    // console.log('onTakePhotoChange');
    const formData = new FormData();
    formData.append('type', this.state.uploadType);
    formData.append('files', e.target.files[0]);

    try {
      const { data, code } = await uploadTicket({ body: formData });
      code == 200 && alert('上传成功');
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  onModalClose = () => {
    this.setState({ showUploader: false }, () => {
      this.setState({
        showTicketDetail: false,
        uploadTicketList: [],
        showUploader: true,
        currentIndex: 0,
      });
    });
  };

  render() {
    const {
      foldTreeData,
      showAddModal,
      showTicketDetail,
      treeNode,
      addSubNode,
      tableList,
      invoiceTypes,
      pagination,
      uploadType,
      uploadTicketList,
      folderId,
      currentIndex,
      uploading,
      showUploader,
    } = this.state;

    const { scanData: uploadTicketInfo = [], basicInfo: dataManageVo = [], ticketEditable, url } =
      uploadTicketList[currentIndex] || {};

    const modalFooter = (
      <>
        <Button onClick={this.onModalClose}>返回</Button>
        <Button onClick={this.onPreTicket} disabled={currentIndex === 0}>
          上一张
        </Button>
        <Button type="primary" onClick={this.onNextTicket}>
          校验完成{currentIndex < uploadTicketList.length - 1 ? '开始下一张' : ''}
        </Button>
      </>
    );

    const menu = (
      <Upload {...this.uploadProps} multiple data={{ type: uploadType }}>
        <Menu>
          {invoiceTypes.map(v => (
            <Menu.Item
              key={v.id}
              onClick={() => this.setState({ uploadType: v.id, uploadTypeName: v.name ,sceneID:v.scene })}
            >
              {v.name}
            </Menu.Item>
          ))}
        </Menu>
      </Upload>
    );

    const takephotoMenu = (
      <ul
        className="ant-menu ant-menu-light ant-menu-root ant-menu-vertical"
        style={{
          boxShadow:
            '0 3px 6px -4px rgb(0 0 0 / 10%), 0 6px 16px 0 rgb(0 0 0 / 8%), 0 9px 28px 8px rgb(0 0 0 / 5%)',
        }}
      >
        {invoiceTypes.map((v, i) => (
          <li
            className="ant-menu-item ant-menu-item-only-child"
            onClick={() => this.setState({ uploadType: v.id })}
            key={i}
          >
            <a href="javascript:;" class="camera-file">
              {v.name}
              <input
                type="file"
                accept="image/"
                capture="camera"
                onChange={this.onTakePhotoChange}
              />
            </a>
          </li>
        ))}
      </ul>
    );

    return (
      <Row className="profile-page">
        <Col span={5}>
          <Button
            type="link"
            className="mb-10"
            onClick={() => this.setState({ showAddModal: true, treeNode: null })}
          >
            +主目录
          </Button>
          <DirectoryTree
            defaultSelectedKeys="0"
            treeData={foldTreeData}
            onSelect={this.onTreeNodeSelect}
          ></DirectoryTree>
        </Col>
        <Col span={18} offset={1}>
          <div className="mb-15 flex-hcb">
            <div></div>
            <div>
              <div className="flex-hc">
                {showUploader && (
                  <Dropdown overlay={menu} placement="bottomCenter" trigger={['click']}>
                    <Button type="primary" className="mr-15">
                      本地上传识别
                    </Button>
                  </Dropdown>
                )}

                {/* <Dropdown overlay={takephotoMenu} placement="bottomCenter" trigger={['click']}>
                  <Button type="primary" className="mr-15">
                    <img src={cameraImg} style={{ width: '20px' }} alt="" />
                    &nbsp;拍照上传识别
                  </Button>
                </Dropdown> */}
                <Upload
                  {...this.uploadProps}
                  data={{ type: 0, folderId }}
                  action="/data-manage/uploadFile"
                  multiple
                >
                  <Button type="primary">+上传资料</Button>
                </Upload>
              </div>
            </div>
          </div>
          <Table
            dataSource={tableList}
            columns={this.columns}
            pagination={pagination}
            onChange={this.onPageChange}
            scroll={{ y: 500 }}
            rowKey="id"
          />
        </Col>
        <Modal
          visible={showAddModal}
          title={treeNode && addSubNode ? '添加子目录' : treeNode ? '编辑' : '添加'}
          onCancel={() => this.setState({ showAddModal: false })}
          onOk={this.handleAddFolder}
          destroyOnClose
        >
          <Form
            labelCol={{ span: 4 }}
            wrapperCol={{ offset: 1 }}
            ref={this.addForm}
            initialValues={{ name: treeNode && !addSubNode ? treeNode.name : '' }}
          >
            <Form.Item name="name" label="目录名称" rules={[{ required: true }]}>
              <Input placeholder="请输入" />
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          visible={showTicketDetail}
          title="票据详情"
          width="80%"
          onCancel={this.onModalClose}
          maskClosable={false}
          wrapClassName="ticket-detail"
          okText={ticketEditable ? '校验完成' : '确定'}
          cancelText="返回"
          onOk={this.handleModalConfirm}
          destroyOnClose
          footer={uploadTicketList.length > 1 ? modalFooter : undefined}
        >
          <Row style={{ maxHeight: '400px' }}>
            <Col span={15} style={{ overflow: 'hidden' }}>
              <div className="">
                <PreviewImage src={url} prefix={IMG_PREFIX} />
              </div>
            </Col>
            <Col style={{ overflowY: 'scroll', maxHeight: '400px' }} span={8} offset={1}>
              <div>
                <h4>校验识别内容</h4>
                <List
                  grid={{ column: 1 }}
                  dataSource={uploadTicketInfo}
                  header={
                    <Row>
                      <Col span={10} className="table-cell">
                        字段名称
                      </Col>
                      <Col span={14} className="table-cell">
                        字段值
                      </Col>
                    </Row>
                  }
                  renderItem={(item, i) => (
                    <Row>
                      <Col span={10} className="table-cell">
                        {item.title}
                      </Col>
                      <Col span={14} className="table-cell">
                        {ticketEditable ? (
                          <Input
                            suffix={<EditOutlined />}
                            value={item.value}
                            onChange={e => this.onTicketInfoInputChange(e.target.value, i)}
                          />
                        ) : (
                          item.value
                        )}
                      </Col>
                    </Row>
                  )}
                />
                <h4 className="mt-20">校验基础信息</h4>
                <List
                  grid={{ column: 1 }}
                  dataSource={ticketEditable ? dataManageVo.filter(v => v.edit) : dataManageVo}
                  header={
                    <Row>
                      <Col span={10} className="table-cell">
                        字段名称
                      </Col>
                      <Col span={14} className="table-cell">
                        字段值
                      </Col>
                    </Row>
                  }
                  renderItem={(item, i) => (
                    <Row>
                      <Col span={10} className="table-cell">
                        {item.title}
                      </Col>
                      <Col span={14} className="table-cell">
                        {item.key === 'groupId' ? (
                          this.renderTreeSelect(i, !ticketEditable)
                        ) : ticketEditable && !['typeName'].includes(item.key) ? (
                          <Input
                            suffix={<EditOutlined />}
                            value={item.value}
                            onChange={e => this.onManageInputChange(e.target.value, i)}
                          />
                        ) : (
                          item.value
                        )}
                      </Col>
                    </Row>
                  )}
                />
              </div>
            </Col>
          </Row>
        </Modal>
        {uploading && (
          <div className="loading">
            <LoadingOutlined />
            <span>上传中，请稍后</span>
          </div>
        )}
      </Row>
    );
  }
}

export default Profile;
