import React, { useEffect, useState, useMemo } from 'react';
import { connect } from 'dva';
import {
  Button,
  Table,
  Form,
  Input,
  Radio,
  Divider,
  message,
  Popconfirm,
  TreeSelect,
  List,
  Row,
  Col,
  Modal,
} from 'antd';

import PreviewImage from '../../components/PreviewImage';
import './style.scss';
const IMG_PREFIX = '/data-manage/image/';

function TicketList(props) {
  const columns = [
    {
      title: '编号',
      dataIndex: 'index',
      key: 'index',
      width: 100,
    },
    {
      title: '票据类型',
      dataIndex: 'typeName',
      key: 'typeName',
    //   render: field => <span>{['上传文件', '电子化识别'][field]}</span>,
      width: 120,
    },
    // 票据的字段
    {
      title: '存放位置',
      dataIndex: 'position',
      key: 'position',
      width: 150,
    },
    {
      title: '状态',
      dataIndex: 'statusName',
      key: 'statusName',
      width: 80,
    },
    {
      title: '归属部门',
      dataIndex: 'groupName',
      key: 'groupName',
      width: 150,
    },
    {
      title: '备注',
      dataIndex: 'description',
      key: 'description',
      width: 150,
    },
    {
      title: '上传人',
      dataIndex: 'createdBy',
      key: 'createdBy',
      width: 150,
    },
    {
      title: '上传时间',
      dataIndex: 'createdTime',
      key: 'createdTime',
      width: 180,
    },
    {
      title: '最近修改人',
      dataIndex: 'updatedBy',
      key: 'updatedBy',
      width: 150,
    },
    {
      title: '最近修改时间',
      dataIndex: 'updatedTime',
      key: 'updatedTime',
      width: 180,
    },
    {
      title: '操作',
      align:'center',
      dataIndex: 'action',
      key: 'action',
      fixed: 'right',
      width: 200,
      render: (field, record) => (
        <span>
          <Button type="link" onClick={() => getInvoiceDetail(record)}>
            详情
          </Button>
          {record.status == 2 ? (
            ''
          ) : (
            <Button type="link" onClick={() => handleDelTicket(record)}>
              删除
            </Button>
          )}
        </span>
      ),
    },
  ];
  const [headers, setHeaders] = useState([]);
  const [tableCol, setTableCol] = useState(columns);
  const [tableList, setTableList] = useState([]);
  const [pagination, setPagination] = useState({ current: 1, total: 0, pageSize: 5 });
  const [treeData, setTreeData] = useState([]);
  const [uploadTicketInfo, setUploadTicketInfo] = useState([]);
  const [detail, setDetail] = useState({});
  const [isFold, setTagFold] = useState(true);
  const [dataManageVo, setDataManageVo] = useState([
    { title: '票据类型', key: 'typeName', value: 1, edit: true },
    // { title: '名称', key: 'name', edit: true },
    { title: '归属部门', key: 'groupName', edit: true },
    { title: '存放位置', key: 'position', edit: true },
    { title: '备注', key: 'description', edit: true },
    { title: '上传人', key: 'createdBy' },
    { title: '上传时间', key: 'createdTime' },
    { title: '最新确认人', key: 'updatedBy' },
    { title: '最新确认时间', key: 'updatedTime' },
  ]);

  const [filterData, setFilterData] = useState({
    groupId: '',
    schemaData: '',
    position: '',
    type: '',
  });
  const [invoiceTypes, setInvoiceTypes] = useState([]);
  const [showDetail, setShowDetail] = useState(false);

  useEffect(() => {
    getInvoiceList();
  }, [pagination.current, filterData]);

  useEffect(() => {
    getInvoiceType();
    getGroupTree();
  }, []);

  const getInvoiceList = async () => {
    try {
      let { records, size, total, current } = await props.dispatch({
        type: 'ticketModel/getInvoiceList',
        payload: {
          size: pagination.pageSize,
          current: pagination.current,
          ...filterData,
          type: filterData.type === 'all' ? '' : filterData.type,
        },
      });

      records = records.map((v, i) => {
        v.index = i + 1;
        v.statusName =
          v.status == 0 ? '已校验' : v.status == 1 ? '已同意' : v.status == 2 ? '已清除' : '';
        const schemaData = JSON.parse(v.schemaData);

        if (i === 0 && headers.length === 0 && filterData.type !== '') {
          const insertColumns = Object.keys(schemaData).map(v => ({
            title: v,
            dataIndex: v,
            key: v,
            width: 150,
          }));

          columns.splice(1, 0, ...insertColumns);
        }
        return { ...v, ...schemaData };
      });
      if (headers.length > 0 && filterData.type !== '') {
        columns.splice(1, 0, ...headers);
      }

      setTableCol([...columns]);
      setTableList(records);
      setPagination({ pageSize: size, total, current });
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  // 部门数据
  const getGroupTree = async () => {
    try {
      const data = await props.dispatch({ type: 'systemModel/getAllGroup', payload: {} });
      processTreeData(data);
      setTreeData(data);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const processTreeData = (datas = [], prefixKey = '') => {
    datas.forEach((item, i) => {
      item.key = `${prefixKey ? prefixKey + '-' : ''}${i}`;
      item.title = item.groupName;
      if (item.children) {
        processTreeData(item.children, item.key);
      }
    });
  };

  const getInvoiceType = async () => {
    try {
      const data = await props.dispatch({
        type: 'profileModel/selectInvoiceType',
        payload: { size: 100 },
      });
      setInvoiceTypes(data.records);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  // 获取详情
  const getInvoiceDetail = async item => {
    try {
      const res = await props.dispatch({
        type: 'ticketModel/getInvoiceDetail',
        payload: { id: item.id },
      });

      const schemaData = JSON.parse(res.schemaData);
      const uploadTicketInfo = Object.entries(schemaData).map(([key, val], i) => ({
        title: key,
        value: val,
      }));
      dataManageVo.forEach(v => (v.value = res[v.key]));

      setDataManageVo(dataManageVo);
      setShowDetail(true);
      setUploadTicketInfo(uploadTicketInfo);
      setDetail(res);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const handleDelTicket = async item => {
    const modal = Modal.confirm({
      title: '提示',
      icon: null,
      content: '是否确认删除?',
      okText: '确认',
      cancelText: '取消',
      onOk: async () => {
        try {
          await props.dispatch({
            type: 'profileModel/deleteTicket',
            payload: { id: item.dataManageId },
          });
          getInvoiceList();
          message.success('删除成功');
          modal.destroy();
        } catch (error) {
          error.message && message.error(error.message);
        }
      },
    });
  };

  const onPageChange = page => {
    setPagination({ ...pagination, current: page.current });
  };

  const onSearch = val => {
    setFilterData({ ...filterData, schemaData: val });
  };

  // 部门
  const renderTreeSelect = useMemo(() => {
    return (
      <TreeSelect
        style={{ width: '260px' }}
        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
        placeholder="请选择部门"
        allowClear
        treeDefaultExpandAll
        onSelect={(value, label, extra) => setFilterData({ ...filterData, groupId: label.groupId })}
        treeData={treeData}
      />
    );
  }, [treeData]);

  const onInputBlur = e => {
    setFilterData({ ...filterData, position: e.target.value });
  };
  const onInputBlurRemarks = e => {
    setFilterData({ ...filterData, description: e.target.value });
  };

  const onTypeChange = e => {
    const { id, schemaData } = e.target.value || {};
    let typeFields = [];
    try {
      typeFields = JSON.parse(schemaData || '[]');
    } catch (error) {
      console.log(error);
    }
    typeFields = Array.isArray(typeFields) ? typeFields : Object.keys(typeFields);
    const headers = typeFields.map(v => ({
      title: v,
      dataIndex: v,
      key: v,
      width: 150,
    }));
    setHeaders(headers.length > 0 ? headers : []);
    setFilterData({ ...filterData, type: e.target.value === 'all' ? '' : id });
  };

  const agreeInvoiceManage = async () => {
    try {
      await props.dispatch({ type: 'ticketModel/agreeInvoiceManage', payload: { id: detail.id } });
      message.success('确认成功');
      getInvoiceList();
      setShowDetail(false);
    } catch (error) {
      error.message && message.error(error.message);
    }
  };

  const foldStyle = { height: '36px', overflow: 'hidden' };

  return (
    <div>
      <div style={{ display: 'flex' }}>
        <span style={{ minWidth: '60px', lineHeight: '30px' }}>票据类型:</span>

        <Radio.Group
          defaultValue="all"
          buttonStyle="solid"
          onChange={onTypeChange}
          style={isFold ? foldStyle : {}}
        >
          <Radio.Button value="all">全部</Radio.Button>
          {invoiceTypes.map(v => (
            <Radio.Button value={v} key={v.id}>
              {v.name}
            </Radio.Button>
          ))}
        </Radio.Group>
        {invoiceTypes.length > 10 && (
          <Button type="link" onClick={() => setTagFold(!isFold)}>
            展开/收起
          </Button>
        )}
      </div>
      <Divider dashed />
      <div className="flex-hcb mb-20">
        <Form layout="inline">
          <Form.Item name="username" label="归属部门">
            {renderTreeSelect}
          </Form.Item>
          <Form.Item name="password" label="存放位置">
            <Input placeholder="请输入" onBlur={onInputBlur} allowClear style={{width:'260px'}}/>
          </Form.Item>
          <Form.Item name="remarks" label="备注">
            <Input placeholder="请输入" onBlur={onInputBlurRemarks} allowClear style={{width:'260px'}}/>
          </Form.Item>
        </Form>
        <Input.Search
          placeholder="请输入搜索内容"
          onSearch={onSearch}
          enterButton
          allowClear
          style={{ width: '400px' }}
        />
      </div>
      <div>
        <Table
          dataSource={tableList}
          columns={tableCol}
          bordered
          pagination={pagination}
          onChange={onPageChange}
          scroll={{ y: 500 }}
        />
      </div>
      <Modal
        visible={showDetail}
        title="票据详情"
        width="80%"
        onCancel={() => setShowDetail(false)}
        maskClosable={false}
        wrapClassName="ticket-detail"
        okText={'同意确认'}
        cancelText="返回"
        onOk={agreeInvoiceManage}
        confirmLoading={props.loading.effects['ticketModel/agreeInvoiceManage']}
        destroyOnClose
      >
        <Row style={{ maxHeight: '400px' }}>
          <Col span={15} style={{ overflow: 'hidden' }}>
            <div className="">
              <PreviewImage src={detail.url} prefix={IMG_PREFIX} />
            </div>
          </Col>
          <Col style={{ overflowY: 'scroll', maxHeight: '400px' }} span={8} offset={1}>
            <h4>校验识别内容</h4>
            <List
              grid={{ column: 1 }}
              dataSource={uploadTicketInfo}
              header={
                <Row>
                  <Col span={10} className="table-cell">
                    字段名称
                  </Col>
                  <Col span={14} className="table-cell">
                    字段值
                  </Col>
                </Row>
              }
              renderItem={(item, i) => (
                <Row>
                  <Col span={10} className="table-cell">
                    {item.title}
                  </Col>
                  <Col span={14} className="table-cell">
                    {item.value}
                  </Col>
                </Row>
              )}
            />
            <h4 className="mt-20">校验基础信息</h4>
            <List
              grid={{ column: 1 }}
              dataSource={dataManageVo}
              header={
                <Row>
                  <Col span={10} className="table-cell">
                    字段名称
                  </Col>
                  <Col span={14} className="table-cell">
                    字段值
                  </Col>
                </Row>
              }
              renderItem={(item, i) => (
                <Row>
                  <Col span={10} className="table-cell">
                    {item.title}
                  </Col>
                  <Col span={14} className="table-cell">
                    {item.key === 'type' ? ['上传文件', '电子化识别'][item.value] : item.value}
                  </Col>
                </Row>
              )}
            />
          </Col>
        </Row>
      </Modal>
    </div>
  );
}

export default connect(({ ticketModel, loading }) => ({ ...ticketModel, loading }))(TicketList);
