import * as Scene from '../api/scene';
import { checkCode } from '../api/request';

const model = {
  namespace: 'sceneModel',
  state: {
    treeData: [],
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
  },
  effects: {
    *getselectSceneInfo({ payload }, { call, put }) {
      const res = yield call(Scene.selectSceneInfo, payload);
      checkCode(res);
      return res.data;
    },
    *saveSceneInfo({ payload }, { call, put }) {
      const res = yield call(Scene.saveSceneInfo, payload);
      checkCode(res);
      return res;
    },
    *copySceneInfo({ payload }, { call, put }) {
      const res = yield call(Scene.copySceneInfo, payload);
      checkCode(res);
      return res;
    },
    *deleteSceneInfo({ payload }, { call, put }) {
      const res = yield call(Scene.deleteSceneInfo, payload);
      checkCode(res);
      return res;
    },
    *selectTemplateInfo({ payload }, { call, put }) {
      const res = yield call(Scene.selectTemplateInfo, payload);
      checkCode(res);
      return res;
    },
    *saveTemplateInfo({ payload }, { call, put }) {
      const res = yield call(Scene.saveTemplateInfo, payload);
      checkCode(res);
      return res;
    },
    *deleteTemplateInfo({ payload }, { call, put }) {
      const res = yield call(Scene.deleteTemplateInfo, payload);
      checkCode(res);
      return res;
    },
    *selectFileInfo({ payload }, { call, put }) {
      const res = yield call(Scene.selectFileInfo, payload);
      checkCode(res);
      return res;
    },
    *getFileInfo({ payload }, { call, put }) {
      const res = yield call(Scene.getFileInfo, payload);
      checkCode(res);
      return res;
    },
    *fileImage({ payload }, { call, put }) {
      const res = yield call(Scene.fileImage, payload);
      checkCode(res);
      return res;
    },
    *saveBatchTemplateItem({ payload }, { call, put }) {
      const res = yield call(Scene.saveBatchTemplateItem, payload);
      checkCode(res);
      return res;
    },
    *deleteTemplateItem({ payload }, { call, put }) {
      const res = yield call(Scene.deleteTemplateItem, payload);
      checkCode(res);
      return res;
    },
    *deleteTemplateItemField({ payload }, { call, put }) {
      const res = yield call(Scene.deleteTemplateItemField, payload);
      checkCode(res);
      return res;
    },
    
    *selectTemplateItem({ payload }, { call, put }) {
      const res = yield call(Scene.selectTemplateItem, payload);
      checkCode(res);
      return res;
    },
    *previewFileField({ payload }, { call, put }) {
      const res = yield call(Scene.previewFileField, payload);
      checkCode(res);
      return res;
    },
    *ocrText({ payload }, { call, put }) {
      const res = yield call(Scene.ocrText, payload);
      checkCode(res);
      return res;
    },
    *getTask({ payload }, { call, put }) {
      const res = yield call(Scene.getTask, payload);
      checkCode(res);
      return res;
    },
    

  },
};

export default model;
