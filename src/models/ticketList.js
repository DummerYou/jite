import * as Service from '../api/index';
import { checkCode } from '../api/request';

const model = {
  namespace: 'ticketModel',
  state: {
    treeData: [],
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
  },
  effects: {
    *getInvoiceList({ payload }, { call, put }) {
      const res = yield call(Service.getInvoiceList, payload);
      checkCode(res);
      return res.data;
    },
    *getInvoiceDetail({ payload }, { call, put }) {
      const res = yield call(Service.getInvoiceDetail, payload);
      checkCode(res);
      return res.data;
    },
    *agreeInvoiceManage({ payload }, { call, put }) {
      const res = yield call(Service.agreeInvoiceManage, payload);
      checkCode(res);
      return res.data;
    },
  },
};

export default model;
