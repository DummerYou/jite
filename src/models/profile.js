import * as Service from '../api/index';
import { checkCode } from '../api/request';

const model = {
  namespace: 'profileModel',
  state: {
    treeData: [],
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
  },
  effects: {
    *getTreeData({ payload }, { call, put }) {
      const res = yield call(Service.selectFolderInfo, payload);
      checkCode(res);
      return res.data;
    },

    *addFolder({ payload }, { call, put }) {
      const res = yield call(Service.addFolder, payload);
      checkCode(res);
      return res.data;
    },

    *delFolder({ payload }, { call, put }) {
      const res = yield call(Service.delFolder, payload);
      checkCode(res);
      return res.data;
    },

    //
    *getTableList({ payload }, { call, put }) {
      const res = yield call(Service.selectDataManage, payload);
      checkCode(res);
      return res.data;
    },

    *selectInvoiceType({ payload }, { call, put }) {
      const res = yield call(Service.selectInvoiceType, payload);
      checkCode(res);
      return res.data;
    },
    *deleteTicket({ payload }, { call, put }) {
      const res = yield call(Service.deleteTicket, payload);
      checkCode(res);
      return res.data;
    },
    *saveInvoiceInfo({ payload }, { call, put }) {
      const res = yield call(Service.saveInvoiceInfo, payload);
      checkCode(res);
      return res.data;
    },

    *getInvoiceDetail({ payload }, { call, put }) {
      const res = yield call(Service.getInvoiceDetail, payload);
      checkCode(res);
      return res.data;
    },
  },
};

export default model;
