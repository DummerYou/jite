const index = {
  namespace: 'index',
  state: {
    menus: null,
    breadcrumb: null,
  },
  reducers: {
    set(state, { payload }) {
      return { ...state, ...payload };
    },
  },
  effects: {
    *fetchSystemMenu({ payload }, { call, put }) {},
  },
};

export default index;
