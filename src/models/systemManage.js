import * as Service from '../api/index';
import { checkCode } from '../api/request';

const model = {
  namespace: 'systemModel',
  state: {
    treeData: [],
  },
  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
  },
  effects: {
    *getAllGroup({ payload }, { call, put }) {
      const res = yield call(Service.getAllGroup, payload);
      checkCode(res);

      return res.data;
    },

    *deleteGroup({ payload }, { call, put }) {
      const res = yield call(Service.deleteGroup, payload);
      checkCode(res);
      return res.data;
    },

    *editGroup({ payload }, { call, put }) {
      const res = yield call(Service.editGroup, payload);
      checkCode(res);
      return res.data;
    },

    //获取部门详情
    *getGroupDetail({ payload }, { call, put }) {
      const res = yield call(Service.getGroupDetail, payload);
      checkCode(res);
      return res.data;
    },

    // 用户相关
    *getUserList({ payload }, { call, put }) {
      const res = yield call(Service.getUserList, payload);
      checkCode(res);
      return res.data;
    },

    *userForbidden({ payload }, { call, put }) {
      const res = yield call(Service.userForbidden, payload);
      checkCode(res);
      return res.data;
    },

    *addUser({ payload }, { call, put }) {
      const res = yield call(Service.addUser, payload);
      checkCode(res);
      return res.data;
    },

    *editUser({ payload }, { call, put }) {
      const res = yield call(Service.editUser, payload);
      checkCode(res);
      return res.data;
    },

    *deleteUser({ payload }, { call, put }) {
      const res = yield call(Service.deleteUser, payload);
      checkCode(res);
      return res.data;
    },

    *resetPassword({ payload }, { call, put }) {
      const res = yield call(Service.resetPassword, payload);
      checkCode(res);
      return res.data;
    },
    *getRoleList({ payload }, { call, put }) {
      const res = yield call(Service.getRoleList, payload);
      checkCode(res);
      return res.data;
    },
  },
};

export default model;
