import React from 'react';
import { Router, Route, Switch, routerRedux } from 'dva/router';
import IndexPage from './pages/profile';
import TicketPage from './pages/ticketList';
import ScenePage from './pages/sceneList';
import ScenePageOperation from './pages/sceneList/operation';
import ScenePageFileList from './pages/sceneList/fileList';
import ScenePageTemplate from './pages/sceneList/template';


import SystemManage from './pages/systemManage';
import LoginPage from './pages/login';
import { Layout, Menu, ConfigProvider, Spin, Avatar } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import zhCN from 'antd/es/locale/zh_CN';
import './index.scss';
import './style.scss';

Spin.setDefaultIndicator(<LoadingOutlined style={{ fontSize: 35 }} spin />);

const { Header, Content } = Layout;

const { ConnectedRouter } = routerRedux;

function AppLayout({ history }) {
  const onLogout = () => {
    window.localStorage.clear();
    window.location.hash = `#/login`;
  };

  return (
    <Layout className="layout">
      <Header style={{ zIndex: 100 }}>
        <Menu mode="horizontal" defaultSelectedKeys={[window.location.hash.slice(1)]}>
          <Menu.Item key="/scene" onClick={() => history.push('/scene')}>
            场景中心
          </Menu.Item>          
          <Menu.Item key="/" onClick={() => history.push('/')}>
            档案中心
          </Menu.Item>
          <Menu.Item key="/ticket" onClick={() => history.push('/ticket')}>
            票据检索
          </Menu.Item>
          <Menu.Item key="/manage" onClick={() => history.push('/manage')}>
            系统管理
          </Menu.Item>
          <Menu.SubMenu
            key="SubMenu"
            title={<Avatar style={{ backgroundColor: '#f56a00' }}>Admin</Avatar>}
          >
            <Menu.Item onClick={onLogout}>退出</Menu.Item>
          </Menu.SubMenu>
        </Menu>
      </Header>
      <Content>
        <div className="site-layout-content">
          <Router history={history}>
            <Switch>
              <Route path="/" exact component={IndexPage} />
              <Route path="/scene" exact component={ScenePage} />
              <Route path="/scene/operation" exact component={ScenePageOperation} />
              <Route path="/scene/fileList" exact component={ScenePageFileList} />
              <Route path="/scene/template" exact component={ScenePageTemplate} />
              <Route path="/ticket" exact component={TicketPage} />
              <Route path="/manage" exact component={SystemManage} />
            </Switch>
          </Router>
        </div>
      </Content>
    </Layout>
  );
}

function RouterConfig({ history, app }) {
  return (
    <ConnectedRouter history={history}>
      <ConfigProvider locale={zhCN}>
        <Switch>
          <Route path="/login" exact component={LoginPage} />
          <Route path="/" component={AppLayout} />
        </Switch>
      </ConfigProvider>
    </ConnectedRouter>
  );
}

export default RouterConfig;
