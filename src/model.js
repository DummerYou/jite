const context = require.context('./models', false, /\.js$/);

// 批量导入model
const getModel = context.keys().map(key => context(key));

export function createModel(app) {
  //根据传进来的APP进行循环加入
  console.log('------getModel--------', getModel);
  return getModel.map(key => app.model(key.default));
}
