import React, { useState, useRef } from 'react';
import { ReloadOutlined, PlusOutlined, MinusOutlined } from '@ant-design/icons';
import './style.scss';

const SCALE_STEP = 10;
export default function PreviewImage(props) {
  const src = props.prefix + props.src;
  const [scale, setScale] = useState(100);
  const [rotate, setRotate] = useState(0);
  const imgRef = useRef();

  const imgStyle = {
    transform: `rotate(${rotate}deg) scale(${scale / 100})`,
  };

  const onMouseDown = ev => {
    ev.preventDefault();
    const disX = ev.clientX - imgRef.current.offsetLeft;
    const disY = ev.clientY - imgRef.current.offsetTop;
    imgRef.current.style.transition = '0s';
    document.onmousemove = function (event) {
      imgRef.current.style.left = event.clientX - disX + 'px';
      imgRef.current.style.top = event.clientY - disY + 'px';
      imgRef.current.style.cursor = 'move';
    };
  };

  const onMouseUp = e => {
    imgRef.current.style.cursor = 'default';
    document.onmousemove = null;
    imgRef.current.style.transition = '0.2s';
  };

  return (
    <div className="preview-image">
      <div className="tools">
        <MinusOutlined
          className="mr-10"
          onClick={() => setScale(scale === 0 ? 0 : scale - SCALE_STEP)}
        />
        <span>{scale}%</span>
        <PlusOutlined className="ml-10 mr-10" onClick={() => setScale(scale + SCALE_STEP)} />
        <ReloadOutlined onClick={() => setRotate(rotate + 90)} />
      </div>
      <img
        src={src}
        alt=""
        className="photo"
        style={imgStyle}
        onMouseDown={onMouseDown}
        ref={imgRef}
        // onMouseMove={onMouseMove}
        onMouseUp={onMouseUp}
      />
    </div>
  );
}
